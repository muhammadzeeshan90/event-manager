<?php 
/*
Plugin Name: Webinar Press
Description: Conduct Your Own Webinar Right On Your Wordpress Platform with Webinar Press.
Version: 1.1.4
*/

if (!defined('DS')) {
    define("DS", DIRECTORY_SEPARATOR);
}

register_activation_hook(__FILE__, 'plugin_activation');

function plugin_activation(){    
    require_once(dirname(__FILE__)  . DS . 'inc' . DS . 'sql' . DS . 'tables.php');
    $settings = array(
            'name' => 'Enter Your Name',
            'email' => 'Enter Your Email',
            'join_event'=> 'Join Event',
            'sticky_messages'=> 'Sticky Messages',
            'start'=> 'Start',
            'end'=> 'End',
            'timezone'=> 'Timezone',
            'chat_room'=> 'Chat Room',
            'public messages'=> 'Public Messages',
            'private messages'=> 'Private Messages',
            'message'=> 'Message',
            'send'=> 'Send',
            'attendee_list'=> 'Attendee List'
            
        );
        
    foreach($settings as $key=>$setting){                
        update_option('placeholder_'.$key, $setting);

    }
}

include(dirname(__FILE__) . DS . 'inc' . DS . 'class.event-manager.php');
include(dirname(__FILE__) . DS . 'inc' . DS . 'class.wp-list.php');
new Event();

include(dirname(__FILE__) . DS . 'ajax-chat.php');
