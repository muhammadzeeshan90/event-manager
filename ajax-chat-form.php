<?php // Simple Ajax Chat > Chat Form

if (!defined('ABSPATH')) exit;

if (sac_is_session_started() === false) {
	
	$simple_ajax_chat_domain = sanitize_text_field($_SERVER['HTTP_HOST']);
	
	session_set_cookie_params('21600', '/', $simple_ajax_chat_domain, false, true);
	
	session_start();
	
}

function sac_is_session_started() {
	
	if (php_sapi_name() !== 'cli') {
		
		if (version_compare(phpversion(), '5.4.0', '>=')) {
			
			return session_status() === PHP_SESSION_ACTIVE ? true : false;
			
		} else {
			
			return session_id() === '' ? false : true;
			
		}
		
	}
	
	return false;
	
}



function event_ajax_chat($event_id, $is_mod, $is_admin, $event_moderators = array(), $all_users) {
	
	global $wpdb, $table_prefix, $sac_options;
	
	$ban = null;
	$silence = null;
	
	if(is_user_logged_in()){
		$current_user = wp_get_current_user();
		$ban = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_user_actions WHERE user_id='.$current_user->ID.' AND event_id='.$event_id.' AND action="ban"');
	}else{
		$ip_address = sac_get_ip_address();
		$ban = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_user_actions WHERE ip='.$ip_address.' AND event_id='.$event_id.' AND action="ban"');
	}
	
	if($ban){
		echo "<p>Forbidden: Access denied, please contact website admistrator.</p>";
		return false;
	}
	if($is_mod == true || $is_admin == true){
		if(isset($_GET['action']) && isset($_GET['user']) && isset($_GET['data']) && isset($_GET['event_id'])){
			$user_id = 0;
			$ip = null;
			if($_GET['user'] == "member"){
				$user_id = $_GET['data'];
			}elseif($_GET['user'] == "guest"){
				$ip = $_GET['data'];
			}
			
			$insert_data = array(
				'user_id' => $user_id,
				'ip' => $ip,
				'event_id' => $_GET['event_id'],
				'action' => $_GET['action']
			);
			
			$wpdb->replace($wpdb->prefix .'weca_user_actions', $insert_data);
		}
	}
	
	$event = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_events WHERE weca_event_id='.$event_id);
	
	$use_url         = isset($sac_options['sac_use_url'])         ? $sac_options['sac_use_url']         : true;
	$use_textarea    = isset($sac_options['sac_use_textarea'])    ? $sac_options['sac_use_textarea']    : true;
	$registered_only = isset($sac_options['sac_registered_only']) ? $sac_options['sac_registered_only'] : false; 
	$enable_styles   = isset($sac_options['sac_enable_style'])    ? $sac_options['sac_enable_style']    : true;
	$play_sound      = isset($sac_options['sac_play_sound'])      ? $sac_options['sac_play_sound']      : true;
	$chat_order      = isset($sac_options['sac_chat_order'])      ? $sac_options['sac_chat_order']      : false;
	$use_username    = true;
	
	$custom_chat_pre = isset($sac_options['sac_content_chat']) ? $sac_options['sac_content_chat'] : '';
	$custom_form_pre = isset($sac_options['sac_content_form']) ? $sac_options['sac_content_form'] : '';
	$custom_chat_app = isset($sac_options['sac_chat_append'])  ? $sac_options['sac_chat_append']  : '';
	$custom_form_app = isset($sac_options['sac_form_append'])  ? $sac_options['sac_form_append']  : '';
	
	$max_chats = isset($sac_options['max_chats']) ? intval($sac_options['max_chats']) : 999;
	
	$display_order = $chat_order ? 'ASC' : 'DESC';
	
	$custom_styles = $enable_styles ? '<style type="text/css">'. $sac_options['sac_custom_styles'] .'</style>' : '';
	
	if (($registered_only && current_user_can('read')) || (!$registered_only)) {

		//$current_user    = wp_get_current_user();
		$logged_username = $current_user->display_name;
		
		if($event->weca_chat_room == "close"){
			if($is_admin == false && $is_mod == false){
				$users = get_users(array("role" => "administrator"));
				foreach($users as $user){
					$admin_user_ids[]=$user->ID;
				}
			
				$merge_array = array_merge($admin_user_ids, $event_moderators);
				$management_members = implode(",", $merge_array);
				
				$statment = "AND text_by_user_id IN ('{$management_members}')";
			}
		}
				
		$results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ". $table_prefix ."weca_chat_log WHERE text_sticky IS NULL AND weca_event_id = '{$event_id}' AND text_type = 'public' ".$statment." ORDER BY text_id DESC LIMIT %d", $max_chats));
		
		$private_results = null;
		if(is_user_logged_in()){
			$private_msg_statment = null;
			if($is_mod == false && $is_admin == false){
				$private_msg_statment = "AND text_by_user_id = '".$current_user->ID."' OR text_to_user_id = '".$current_user->ID."'";
			}
			$private_results = $wpdb->get_results($wpdb->prepare("SELECT * FROM ". $table_prefix ."weca_chat_log WHERE text_sticky IS NULL AND weca_event_id = '{$event_id}' AND text_type='private' ".$private_msg_statment." ORDER BY text_id DESC LIMIT %d", $max_chats));
		}
		
		echo '<div id="simple-ajax-chat" data-event-id="'.$event_id.'">';
		
		echo $custom_chat_pre;
		
		echo '<div id="sac-content"></div>';
		?>
		
		<ul class="custom_tabs">
			<li class="active public_tab" data-tab="sac-messages">Public</li>
			<?php
			if($is_mod == true || $is_admin == true){
			?>
				<li data-tab="sac-private-messages" class="private_tab">Private</li>
			<?php
			}elseif(is_user_logged_in()){
			?>
				<li data-tab="sac-private-messages" class="private_tab">Private</li>
			<?php
			}
			?>
		</ul>
		<div style="clear: both;"></div>
		
		<?php
		echo '<div id="sac-output">';
		
		$sac_first_time = true;
		$sac_private_first_time = true;
		$sac_output     = '';
		$sac_private_output     = '';
		$sac_lastout    = '';
		$sac_private_lastout    = '';
		$lastID         = null;
			
		if ($results) {
			
			foreach($results as $r) {
				
				$chat_text = sanitize_text_field($r->text_message);
				$chat_time = sanitize_text_field($r->text_datetime);
				$chat_id   = sanitize_text_field($r->text_id);
				$chat_url  = null;
				
				if($r->text_by_user_id > 0){
					$user_info = get_userdata($r->text_by_user_id);
					$chat_name = sanitize_text_field($user_info->user_login);					
				}else{
					$chat_name = sanitize_text_field($r->name);
				}
								
				$mod_message = false;
				$admin_message = false;
				$make_bold = false;
				
				if($r->text_by_user_id > 0){
					if(in_array($r->text_by_user_id, (array)$event_moderators)){
						$mod_message = true;
						$make_bold = true;
					}
					
					if($mod_message == false){
						if(in_array($r->text_by_user_id, $user_info->roles)){
							$admin_message = true;
							$make_bold = true;
						}
					}
				}
				
				$name_class = preg_replace("/[\s]+/", "-", $chat_name);
				
				$pattern = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
				$chat_text = preg_replace($pattern, '<a target="_blank" href="\\0" title="Open link in new tab">\\0</a>', $chat_text);
				
				if ($sac_first_time === true) $lastID = $chat_id;
				
				if (empty($chat_url) || $chat_url == 'http://' || $chat_url == 'https://') {
					
					$url = $chat_name;
					
				} else {
					
					$url = '<a target="_blank" href="'. $chat_url .'">'. $chat_name .'</a>';
					
				}
				
				$sac_out  = '<li class="sac-chat-message sac-static sac-user-'. $name_class .'" data-time="'. date('Y-m-d,H:i:s', $chat_time) .'">'. "\n";
				$sac_out .= '<span title="'. sprintf(esc_attr__('Posted %s ago', 'simple-ajax-chat'), sac_time_since($chat_time)) .'">'. $url .' : </span> ';
				
				if($make_bold == true){
					$sac_out  .= '<b>';
				}
				
				$sac_out .= convert_smilies(' '. $chat_text) .''. "\n";
				
				if($make_bold == true){
					$sac_out  .= '</b>';
				}
				
				$sac_out .= '</li>';
				
				if($is_admin == true || $is_mod == true){
					if($r->text_by_user_id != $current_user->ID){
						if($r->text_by_user_id > 0){
							$user_type = "member";
							$data = $r->text_by_user_id;
						}else{
							$user_type = "guest";
							$data = $r->ip;
						}
						$sac_out .= '<p class="text_admin_menu"><small><a href="?action=ban&user='.$user_type.'&data='.$data.'">Ban</a> | <a href="?action=silence&user='.$user_type.'&data='.$data.'">Silence</a></small></p>';
					}
				}
				
				if ($chat_order) $sac_output  = $sac_out . $sac_output;
				else             $sac_output .= $sac_out;
				
				$sac_first_time = false;
				
			}
			
		}
		
		if ($private_results) {
			
			foreach($private_results as $r) {
				
				$chat_text = sanitize_text_field($r->text_message);
				$chat_time = sanitize_text_field($r->text_datetime);
				$chat_id   = sanitize_text_field($r->text_id);
				$chat_url  = null;
				
				if($r->text_by_user_id > 0){
					$user_info = get_userdata($r->text_by_user_id);
					$chat_name = sanitize_text_field($user_info->user_login);
				}else{
					$chat_name = sanitize_text_field($r->name);
				}
								
				
				$name_class = preg_replace("/[\s]+/", "-", $chat_name);
				
				$pattern = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
				$chat_text = preg_replace($pattern, '<a target="_blank" href="\\0" title="Open link in new tab">\\0</a>', $chat_text);
				
				if ($sac_private_first_time === true) $lastID = $chat_id;
				
				$url = $chat_name;
				
				$sac_private_out  = '<li class="sac-chat-message sac-static sac-user-'. $name_class .'" data-time="'. date('Y-m-d,H:i:s', $chat_time) .'">'. "\n";
				$sac_private_out .= '<span title="'. sprintf(esc_attr__('Posted %s ago', 'simple-ajax-chat'), sac_time_since($chat_time)) .'">'. $url .' : </span>';
				
				$sac_private_out .= convert_smilies(' '. $chat_text) .'</li>'. "\n";
				
				if($is_admin == true || $is_mod == true){
					if($r->text_by_user_id != $current_user->ID){
						if(is_user_logged_in()){
							$user_type = "member";
							$data = $r->text_by_user_id;
						}else{
							$user_type = "guest";
							$data = $r->ip;
						}
						$sac_private_out .= '<p class="text_admin_menu"><small><a href="?action=ban&user='.$user_type.'&data='.$data.'">Ban</a> | <a href="?action=silence&user='.$user_type.'&data='.$data.'">Silence</a></small></p>';
					}
				}
				
				if ($chat_order) $sac_private_output  = $sac_private_out . $sac_private_output;
				else             $sac_private_output .= $sac_private_out;
				
				$sac_private_first_time = false;
				
			}
			
		}
		
		echo $sac_lastout .'<ul id="sac-messages">'. "\n" . $sac_output .'</ul>'. "\n";
		if(is_user_logged_in()){
			echo $sac_private_lastout .'<ul id="sac-private-messages" style="display: none">'. "\n" . $sac_private_output .'</ul>'. "\n";
		}

		echo '</div>';
		
		echo $custom_chat_app;
		echo $custom_form_pre; 
		
		?>
		
		<div id="sac-panel">
			<input id="current_room_status" type="hidden" value="<?php echo $event->weca_chat_room; ?>">
			<?php
			if($event->weca_chat_room == "open" || $is_mod == true || $is_admin == true){
				$style='';
				$message_style='style="display: none;"';
			}else{
				$style='style="display: none;"';	
				$message_style='';
			}
			
			echo '<div '.$message_style.' class="chat_room_closed_message">Chat room is closed</div>';
			?>
			<form id="sac-form" <?php echo $style; ?> method="post" action="<?php echo plugins_url('/event-manager/ajax-chat-core.php'); ?>">
				
				<input type="hidden" name="sac_event_id" id="sac_event_id" value="<?php echo $event_id; ?>">
				
				<?php if ($use_username && !empty($logged_username)) : ?>
				
				<fieldset id="sac-user-info">
					<label for="sac_name"><?php esc_html_e('Name', 'simple-ajax-chat'); ?>: <span><?php echo $logged_username; ?></span></label>
					<input type="hidden" name="sac_name" id="sac_name" value="<?php echo $logged_username; ?>" />
				</fieldset>
				
				<?php else : 
					$cookie_username = '';
					if (isset($_COOKIE['sacUserName']) && !empty($_COOKIE['sacUserName'])) $cookie_username = sanitize_text_field(stripslashes($_COOKIE['sacUserName'])); 
				?>
				
				<fieldset id="sac-user-info">
					<label for="sac_name"><?php esc_html_e('Name', 'simple-ajax-chat'); ?>: </label>
					<input type="text" name="sac_name" id="sac_name" value="<?php echo $cookie_username; ?>" placeholder="<?php esc_attr_e('Name', 'simple-ajax-chat'); ?>" />
				</fieldset>
				
				<?php endif;
				
				$cookie_url = 'http://';
				if (isset($_COOKIE['sacUrl']) && !empty($_COOKIE['sacUrl'])) $cookie_url = sanitize_text_field($_COOKIE['sacUrl']); 
				
				if (!$use_url) echo '<div style="display:none;">'; ?>
				
				<input type="hidden" name="sac_url" id="sac_url" value="<?php echo $cookie_url; ?>" placeholder="<?php esc_attr_e('URL', 'simple-ajax-chat'); ?>" />
				
				<?php if (!$use_url) echo '</div>'; ?>
				
				<?php
				if(is_user_logged_in()){
				?>
					<fieldset id="sac-user-info">
						<label for="sac_text_type"><?php esc_html_e('Status', 'simple-ajax-chat'); ?>: </label>
						<select name="sac_text_type" id="sac_text_type">
							<option value="public">Public</option>
							<?php
							if($is_mod == true || $is_admin == true){
								echo '<option value="private_to_user">Private Message to user</option>';
							}else{
								echo '<option value="private">Private</option>';
							}
							?>
						</select>					
					</fieldset>
				<?php
					if($is_mod == true || $is_admin == true){
					?>
					<fieldset id="sac-user-list" style="display: none">
						<label for="sac_text_type"><?php esc_html_e('Select User', 'simple-ajax-chat'); ?>: </label>
						<select name="text_to_user_id" id="text_to_user_id">
							<?php
							foreach($all_users as $user){
								echo '<option value="'.$user->ID.'">'.$user->user_login.' ('.$user->display_name.')</option>';
							}
							?>
						</select>					
					</fieldset>
					<?php
					}
				}else{
					?>
					<input type="hidden" id="sac_text_type" value="public" name="sac_text_type" />
					<?php
				}
				?>
				
				<fieldset id="sac-user-chat">
					<label for="sac_chat"><?php esc_html_e('Message', 'simple-ajax-chat') ?>: </label>
					<?php if ($use_textarea) : ?>
					
					<textarea name="sac_chat" id="sac_chat" rows="5" cols="50" onkeypress="if (typeof pressedEnter == 'function') return pressedEnter(this,event);" placeholder="<?php esc_attr_e('Message', 'simple-ajax-chat') ?>"></textarea>
					<?php else : ?>
					
					<input type="text" name="sac_chat" id="sac_chat" />
					<?php endif; ?>
					
				</fieldset>
				
				<fieldset id="sac_verify" style="display:none;height:0;width:0;">
					<label for="sac_verify"><?php esc_html_e('Human verification: leave this field empty.', 'simple-ajax-chat'); ?></label>
					<input name="sac_verify" type="text" size="33" maxlength="99" value="" />
				</fieldset>
				
				<div id="sac-user-submit">					
					<input type="submit" id="submitchat" name="submit" class="submit" value="<?php esc_attr_e('Send', 'simple-ajax-chat'); ?>" />
					<input type="hidden" id="sac_lastID" value="<?php echo $lastID + 1; ?>" name="sac_lastID" />
					<input type="hidden" id="chat_room_status" value="<?php echo $event->weca_chat_room; ?>" name="chat_room_status" />
					<input type="hidden" name="sac_no_js" value="true" />
					<input type="hidden" name="PHPSESSID" value="<?php echo session_id(); ?>" />
					<input type="hidden" name="is_admin" id="is_admin" value="<?php echo $is_admin; ?>" />
					<input type="hidden" name="is_mod" id="is_mod" value="<?php echo $is_mod; ?>" />
					<input type="hidden" name="event_moderators" id="event_moderators" value="<?php echo implode(",", $event_moderators); ?>" />
					
					<?php wp_nonce_field('sac_nonce', 'sac_nonce', false); ?>
				</div>
			</form>
			<script>(function(){var e = document.getElementById("sac_verify");e.parentNode.removeChild(e);})();</script>
			<!-- Simple Ajax Chat @ https://perishablepress.com/simple-ajax-chat/ -->			
		</div>
		
		<?php
		
		echo $custom_form_app;
		
		if ($play_sound == true) : 
			
			$res_path = plugins_url('/event-manager/resources/audio/'); ?>
			
			<audio id="TheBox">
				<source src="<?php echo $res_path; ?>msg.mp3"></source>
				<source src="<?php echo $res_path; ?>msg.ogg"></source>
				<!-- your browser does not support audio -->
			</audio>
			
		<?php endif;
		
	} else { // login required
		
		echo $custom_form_pre; ?>
		
		<div id="sac-panel" class="sac-reg-req">
			<p><?php esc_html_e('You must be a registered user to participate in this chat.', 'simple-ajax-chat'); ?></p>
			<!--p>Please <a href="<?php wp_login_url(get_permalink()); ?>">Log in</a> to chat.</p-->
		</div>
		
		<?php echo $custom_form_app;
		
	}
	
	echo '</div>';
	echo $custom_styles;
	
}


