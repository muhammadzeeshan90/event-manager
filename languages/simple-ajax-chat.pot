#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Simple Ajax Chat\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-11-17 17:35+0000\n"
"POT-Revision-Date: Fri Aug 12 2016 15:57:35 GMT-0700 (PDT)\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: ..\n"
"X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;"
"__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;"
"_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;"
"esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;"
"esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2\n"
"X-Generator: Loco - https://localise.biz/"

#: simple-ajax-chat.php:67
msgid "requires WordPress "
msgstr ""

#: simple-ajax-chat.php:68
msgid " or higher, and has been deactivated! "
msgstr ""

#: simple-ajax-chat.php:69
msgid "Please return to the"
msgstr ""

#: simple-ajax-chat.php:70
msgid "WordPress Admin area"
msgstr ""

#: simple-ajax-chat.php:70
msgid "to upgrade WordPress and try again."
msgstr ""

#: simple-ajax-chat.php:116
msgid "The Admin"
msgstr ""

#: simple-ajax-chat.php:117
msgid "High five! You&rsquo;ve successfully installed Simple Ajax Chat."
msgstr ""

#: simple-ajax-chat.php:159
msgid "ago"
msgstr ""

#: simple-ajax-chat.php:280
msgid "Anonymous"
msgstr ""

#: simple-ajax-chat.php:427 simple-ajax-chat-admin.php:22
msgid "Welcome to the Chat Forum"
msgstr ""

#. Name of the plugin
#: simple-ajax-chat.php:428 simple-ajax-chat-admin.php:23
msgid "Simple Ajax Chat"
msgstr ""

#: simple-ajax-chat.php:447
msgid "Successfully truncated SAC table."
msgstr ""

#: simple-ajax-chat.php:448
msgid "Unable to truncate SAC table."
msgstr ""

#: simple-ajax-chat.php:449
msgid "Truncate not needed, zero rows affected."
msgstr ""

#: simple-ajax-chat.php:451
msgid "Successfully inserted default message."
msgstr ""

#: simple-ajax-chat.php:452
msgid "Unable to insert default message."
msgstr ""

#: simple-ajax-chat.php:453
msgid "Default message not inserted."
msgstr ""

#: simple-ajax-chat.php:466
msgid "Visit SAC Settings"
msgstr ""

#: simple-ajax-chat.php:466
msgid "Settings"
msgstr ""

#: simple-ajax-chat.php:481
msgid "Give us a 5-star rating at WordPress.org"
msgstr ""

#: simple-ajax-chat.php:482
msgid "Rate this plugin"
msgstr ""

#: simple-ajax-chat.php:614
msgid "year"
msgstr ""

#: simple-ajax-chat.php:615
msgid "month"
msgstr ""

#: simple-ajax-chat.php:616
msgid "week"
msgstr ""

#: simple-ajax-chat.php:617
msgid "day"
msgstr ""

#: simple-ajax-chat.php:618
msgid "hour"
msgstr ""

#: simple-ajax-chat.php:619
msgid "minute"
msgstr ""

#: simple-ajax-chat.php:639 simple-ajax-chat.php:648
msgid "s"
msgstr ""

#: ajax-chat-core.php:8
msgid "Please do not load this page directly. Thanks!"
msgstr ""

#: ajax-chat-core.php:83
msgid "Name and comment required."
msgstr ""

#: simple-ajax-chat-admin.php:234
msgid "Currently there"
msgstr ""

#: simple-ajax-chat-admin.php:238
#, php-format
msgid " is %s"
msgid_plural " are %s"
msgstr[0] ""
msgstr[1] ""

#: simple-ajax-chat-admin.php:239
msgid " chat message (your default message)"
msgid_plural " chat messages"
msgstr[0] ""
msgstr[1] ""

#: simple-ajax-chat-admin.php:241
msgid " are 0 chat messages. Please add at least one message via the chat box."
msgstr ""

#: simple-ajax-chat-admin.php:298
msgid "Comment deleted successfully."
msgstr ""

#: simple-ajax-chat-admin.php:299
msgid "Comment edited successfully."
msgstr ""

#: simple-ajax-chat-admin.php:300
msgid "All chat messages deleted."
msgstr ""

#: simple-ajax-chat-admin.php:301
msgid "Default settings restored."
msgstr ""

#: simple-ajax-chat-admin.php:309
msgid "Toggle all panels"
msgstr ""

#: simple-ajax-chat-admin.php:319
msgid "Simple Ajax Chat needs your support!"
msgstr ""

#: simple-ajax-chat-admin.php:323
msgid "Please"
msgstr ""

#: simple-ajax-chat-admin.php:323
msgid "Make a donation via PayPal!"
msgstr ""

#: simple-ajax-chat-admin.php:323
msgid "make a donation"
msgstr ""

#: simple-ajax-chat-admin.php:323
msgid "and/or"
msgstr ""

#: simple-ajax-chat-admin.php:324 simple-ajax-chat-admin.php:355
msgid "Thank you for your support!"
msgstr ""

#: simple-ajax-chat-admin.php:324 simple-ajax-chat-admin.php:355
msgid "give it a 5-star rating"
msgstr ""

#: simple-ajax-chat-admin.php:327
msgid ""
"Your generous support enables continued development of this free plugin. "
"Thank you!"
msgstr ""

#: simple-ajax-chat-admin.php:332
msgid "Check this box if you have shown support"
msgstr ""

#: simple-ajax-chat-admin.php:340
msgid "Overview"
msgstr ""

#: simple-ajax-chat-admin.php:344
msgid "(SAC) displays an Ajax-powered chat box anywhere on your site."
msgstr ""

#: simple-ajax-chat-admin.php:345
msgid ""
"Use the shortcode to display the chat box on any post or page, or use the "
"template tag to display anywhere in your theme."
msgstr ""

#: simple-ajax-chat-admin.php:348 simple-ajax-chat-admin.php:362
msgid "Plugin Settings"
msgstr ""

#: simple-ajax-chat-admin.php:349 simple-ajax-chat-admin.php:621
msgid "Shortcode &amp; Template Tag"
msgstr ""

#: simple-ajax-chat-admin.php:350 simple-ajax-chat-admin.php:636
msgid "Manage Chat Messages"
msgstr ""

#: simple-ajax-chat-admin.php:351 simple-ajax-chat-admin.php:771
msgid "Plugin Homepage"
msgstr ""

#: simple-ajax-chat-admin.php:354
msgid "If you like this plugin, please"
msgstr ""

#: simple-ajax-chat-admin.php:368
msgid ""
"Here you may customize Simple Ajax Chat to suit your needs. Note: after "
"updating time and color options, you may need to refresh/empty the browser "
"cache before you see the changes take effect."
msgstr ""

#: simple-ajax-chat-admin.php:370
msgid "General options"
msgstr ""

#: simple-ajax-chat-admin.php:374
msgid "Default name"
msgstr ""

#: simple-ajax-chat-admin.php:378
msgid ""
"Default name for &ldquo;welcome&rdquo; message. Reset chat messages for new "
"name to be displayed."
msgstr ""

#: simple-ajax-chat-admin.php:383
msgid "Default message"
msgstr ""

#: simple-ajax-chat-admin.php:387
msgid ""
"Default &ldquo;welcome&rdquo; message that appears as the first chat comment."
msgstr ""

#: simple-ajax-chat-admin.php:388
msgid "Reset chat messages for new welcome message to be displayed."
msgstr ""

#: simple-ajax-chat-admin.php:393
msgid "Require log in"
msgstr ""

#: simple-ajax-chat-admin.php:397
msgid "Require users to be logged in to view and use the chat box."
msgstr ""

#: simple-ajax-chat-admin.php:402
msgid "Logged-in username"
msgstr ""

#: simple-ajax-chat-admin.php:406
msgid "Use the logged-in username as the chat name."
msgstr ""

#: simple-ajax-chat-admin.php:411
msgid "Linked username"
msgstr ""

#: simple-ajax-chat-admin.php:415
msgid "Enable users to specify a URL for their chat name."
msgstr ""

#: simple-ajax-chat-admin.php:420
msgid "Large input field"
msgstr ""

#: simple-ajax-chat-admin.php:424
msgid "Display a larger input field for chat messages."
msgstr ""

#: simple-ajax-chat-admin.php:429
msgid "Sound alerts"
msgstr ""

#: simple-ajax-chat-admin.php:433
msgid "Play sound alert for new chat messages."
msgstr ""

#: simple-ajax-chat-admin.php:434
msgid "See the FAQs to learn how to customize the sound alert."
msgstr ""

#: simple-ajax-chat-admin.php:439
msgid "Chat order"
msgstr ""

#: simple-ajax-chat-admin.php:443
msgid ""
"Display chats in ascending order (new messages appear at the bottom of the "
"list). Requires jQuery."
msgstr ""

#: simple-ajax-chat-admin.php:448
msgid "Max chats"
msgstr ""

#: simple-ajax-chat-admin.php:452
msgid "Maximum number of chats that should be allowed in the chat box."
msgstr ""

#: simple-ajax-chat-admin.php:457
msgid "Max characters"
msgstr ""

#: simple-ajax-chat-admin.php:461
msgid ""
"Maximum number of characters that should be allowed in each chat message."
msgstr ""

#: simple-ajax-chat-admin.php:466
msgid "Username length"
msgstr ""

#: simple-ajax-chat-admin.php:470
msgid "Maximum number of characters that should be allowed in the username."
msgstr ""

#: simple-ajax-chat-admin.php:477
msgid "Times and colors"
msgstr ""

#: simple-ajax-chat-admin.php:481
msgid "Update interval"
msgstr ""

#: simple-ajax-chat-admin.php:485
msgid "Refresh frequency (in milliseconds, decimals allowed)."
msgstr ""

#: simple-ajax-chat-admin.php:486
msgid ""
"Smaller numbers make new chat messages appear faster, but also increase "
"server load."
msgstr ""

#: simple-ajax-chat-admin.php:487
msgid "The default is 3 seconds (3000 ms)."
msgstr ""

#: simple-ajax-chat-admin.php:492
msgid "Fade duration"
msgstr ""

#: simple-ajax-chat-admin.php:496
msgid ""
"Fade-duration of most recent chat message (in milliseconds, decimals allowed)"
"."
msgstr ""

#: simple-ajax-chat-admin.php:497
msgid "Default is 1.5 seconds (1500 ms)."
msgstr ""

#: simple-ajax-chat-admin.php:502
msgid "Highlight fade (from)"
msgstr ""

#: simple-ajax-chat-admin.php:506
msgid "&ldquo;Fade-in&rdquo; background-color of new chat messages."
msgstr ""

#: simple-ajax-chat-admin.php:507
msgid "Color must be 6-digit-hex format, default color is #ffffcc."
msgstr ""

#: simple-ajax-chat-admin.php:512
msgid "Highlight fade (to)"
msgstr ""

#: simple-ajax-chat-admin.php:516
msgid "&ldquo;Fade-out&rdquo; background-color of new chat messages."
msgstr ""

#: simple-ajax-chat-admin.php:517
msgid "Color must be 6-digit-hex format, default color is #ffffff."
msgstr ""

#: simple-ajax-chat-admin.php:524
msgid "Appearance"
msgstr ""

#: simple-ajax-chat-admin.php:528
msgid "Enable custom styles?"
msgstr ""

#: simple-ajax-chat-admin.php:532
msgid "Check this box if you want to enable the Custom CSS styles."
msgstr ""

#: simple-ajax-chat-admin.php:537
msgid "Custom CSS styles"
msgstr ""

#: simple-ajax-chat-admin.php:541
msgid "Optional CSS to style the chat form. Do not include"
msgstr ""

#: simple-ajax-chat-admin.php:542
msgid "tags. Check out"
msgstr ""

#: simple-ajax-chat-admin.php:543
msgid "for a complete list of CSS selectors."
msgstr ""

#: simple-ajax-chat-admin.php:550
msgid "Targeted loading"
msgstr ""

#: simple-ajax-chat-admin.php:554
msgid "Chat URL"
msgstr ""

#: simple-ajax-chat-admin.php:558
msgid "By default, SAC JavaScript is included on *every* page."
msgstr ""

#: simple-ajax-chat-admin.php:559
msgid ""
"To prevent this, and to include the required JavaScript only on the chat "
"page, enter its URL here."
msgstr ""

#: simple-ajax-chat-admin.php:560
msgid "Separate multiple URLs with a comma. Leave blank to disable."
msgstr ""

#: simple-ajax-chat-admin.php:567
msgid "Custom content"
msgstr ""

#: simple-ajax-chat-admin.php:571
msgid "Before chat box"
msgstr ""

#: simple-ajax-chat-admin.php:575
msgid ""
"Optional custom content to appear *before* the chat box. Leave blank to "
"disable."
msgstr ""

#: simple-ajax-chat-admin.php:580
msgid "After chat box"
msgstr ""

#: simple-ajax-chat-admin.php:584
msgid ""
"Optional custom content to appear *after* the chat box. Leave blank to "
"disable."
msgstr ""

#: simple-ajax-chat-admin.php:589
msgid "Before chat form"
msgstr ""

#: simple-ajax-chat-admin.php:593
msgid ""
"Optional custom content to appear *before* the chat form. Leave blank to "
"disable."
msgstr ""

#: simple-ajax-chat-admin.php:598
msgid "After chat form"
msgstr ""

#: simple-ajax-chat-admin.php:602
msgid ""
"Optional custom content to appear *after* the chat form. Leave blank to "
"disable."
msgstr ""

#: simple-ajax-chat-admin.php:609 simple-ajax-chat-admin.php:728
msgid "Save Settings"
msgstr ""

#: simple-ajax-chat-admin.php:624
msgid "Shortcode"
msgstr ""

#: simple-ajax-chat-admin.php:625
msgid "Use this shortcode to display the chat box on any WP Post or Page:"
msgstr ""

#: simple-ajax-chat-admin.php:628
msgid "Template tag"
msgstr ""

#: simple-ajax-chat-admin.php:629
msgid ""
"Use this template tag to display the chat box anywhere in your theme "
"template:"
msgstr ""

#: simple-ajax-chat-admin.php:645
msgid "Here is a static list of all chat messages for editing and/or deleting."
msgstr ""

#: simple-ajax-chat-admin.php:646
msgid ""
"Note that you must have at least *one message* in the chat box at all times."
msgstr ""

#: simple-ajax-chat-admin.php:647
msgid ""
"Clicking &ldquo;Delete all chat messages&rdquo; will clear the database and "
"add the default message."
msgstr ""

#: simple-ajax-chat-admin.php:656
msgid "You must have at least one message in the chat box at all times!"
msgstr ""

#: simple-ajax-chat-admin.php:657
msgid "Go post a few chat messages and try again."
msgstr ""

#: simple-ajax-chat-admin.php:673
#, php-format
msgid "Last Message: %s ago"
msgstr ""

#: simple-ajax-chat-admin.php:682
msgid "Delete"
msgstr ""

#: simple-ajax-chat-admin.php:683
msgid "Edit"
msgstr ""

#: simple-ajax-chat-admin.php:695
msgid "Delete all chats"
msgstr ""

#: simple-ajax-chat-admin.php:701
msgid "Banned Phrases"
msgstr ""

#: simple-ajax-chat-admin.php:710
msgid ""
"Comma-separated List of banned words/phrases that never will be displayed in "
"the chat box."
msgstr ""

#: simple-ajax-chat-admin.php:711
msgid "This setting applies to usernames, URLs, and chat messages."
msgstr ""

#: simple-ajax-chat-admin.php:723
msgid "Banned phrases"
msgstr ""

#: simple-ajax-chat-admin.php:734
msgid "Restore Defaults"
msgstr ""

#: simple-ajax-chat-admin.php:744
msgid "Click the button to restore plugin options to their default setttings."
msgstr ""

#: simple-ajax-chat-admin.php:747
msgid "Restore default settings"
msgstr ""

#: simple-ajax-chat-admin.php:753
msgid "Delete all plugin settings"
msgstr ""

#: simple-ajax-chat-admin.php:754
msgid ""
"To delete all plugin settings and chat messages from the database, simply "
"uninstall (delete) the plugin."
msgstr ""

#: simple-ajax-chat-admin.php:759
msgid "Show Support"
msgstr ""

#: simple-ajax-chat-admin.php:771
msgid "by"
msgstr ""

#: simple-ajax-chat-admin.php:772
msgid "Jeff Starr on Twitter"
msgstr ""

#: simple-ajax-chat-admin.php:773
msgid "Obsessive Web Design &amp; Development"
msgstr ""

#: simple-ajax-chat-admin.php:812
msgid ""
"Are you sure you want to delete all chat messages? (this action cannot be "
"undone)"
msgstr ""

#: simple-ajax-chat-admin.php:820
msgid ""
"Are you sure you want to restore default settings? (this action cannot be "
"undone)"
msgstr ""

#: simple-ajax-chat-form.php:108
msgid "Latest Message:"
msgstr ""

#: simple-ajax-chat-form.php:109
#, php-format
msgid "%s ago"
msgstr ""

#: simple-ajax-chat-form.php:114
#, php-format
msgid "Posted %s ago"
msgstr ""

#: simple-ajax-chat-form.php:126
msgid "You need at least one entry in the chat forum!"
msgstr ""

#: simple-ajax-chat-form.php:145 simple-ajax-chat-form.php:155 
#: simple-ajax-chat-form.php:156
msgid "Name"
msgstr ""

#: simple-ajax-chat-form.php:167 simple-ajax-chat-form.php:168
msgid "URL"
msgstr ""

#: simple-ajax-chat-form.php:174 simple-ajax-chat-form.php:177
msgid "Message"
msgstr ""

#: simple-ajax-chat-form.php:186
msgid "Human verification: leave this field empty."
msgstr ""

#: simple-ajax-chat-form.php:191
msgid "Submit"
msgstr ""

#: simple-ajax-chat-form.php:221
msgid "You must be a registered user to participate in this chat."
msgstr ""

#: resources/custom.php:427
msgid "Posted:"
msgstr ""

#. Description of the plugin
msgid ""
"Displays a fully customizable Ajax-powered chat box anywhere on your site."
msgstr ""

#. URI of the plugin
msgid "https://perishablepress.com/simple-ajax-chat/"
msgstr ""

#. Author of the plugin
msgid "Jeff Starr"
msgstr ""

#. Author URI of the plugin
msgid "https://plugin-planet.com/"
msgstr ""
