<?php
if (!defined('ABSPATH')) exit;

$sac_wp_vers   = '4.1';
$sac_version   = '20161116';
$sac_plugin    = 'Event Chat';
$sac_homeurl   = 'http://solzsoft.com/';

$sac_lastID    = isset($_GET['sac_lastID'])  ? $_GET['sac_lastID']  : '';
$sac_eventID    = isset($_GET['event_id'])  ? $_GET['event_id']  : '';
$chat_room_status    = isset($_GET['chat_room_status'])  ? $_GET['chat_room_status']  : '';
$is_admin    = isset($_GET['is_admin'])  ? $_GET['is_admin']  : false;
$is_mod    = isset($_GET['is_mod'])  ? $_GET['is_mod']  : false;
$event_moderators    = isset($_GET['event_moderators'])  ? $_GET['event_moderators']  : null;
$sacGetChat    = isset($_GET['sacGetChat'])  ? $_GET['sacGetChat']  : '';
$sacSendChat   = isset($_GET['sacSendChat']) ? $_GET['sacSendChat'] : '';

$sac_user_name = isset($_POST['n']) ? $_POST['n'] : '';
$sac_user_text = isset($_POST['c']) ? $_POST['c'] : '';
$sac_user_url  = isset($_POST['u']) ? $_POST['u'] : '';

require_once(dirname(__FILE__) .'/ajax-chat-admin.php');
require_once(dirname(__FILE__) .'/ajax-chat-form.php');

$sac_options = get_option('sac_options', sac_default_options());


// get chats
function sac_getData($sac_lastID) {
	global $wpdb, $table_prefix, $sac_lastID, $sacGetChat, $sac_eventID, $chat_room_status, $is_admin, $is_mod, $event_moderators;
	$loop = ''; 
	
	if (isset($_GET['sac_nonce_receive']) && wp_verify_nonce($_GET['sac_nonce_receive'], 'sac_nonce_receive')) {
		
		if ((isset($sacGetChat) && $sacGetChat === 'yes')) {
			
			if($chat_room_status == "close"){
				if($is_admin == false && $is_mod == false){
					$users = get_users(array("role" => "administrator"));
					foreach($users as $user){
						$admin_user_ids[]=$user->ID;
					}
					
					$event_moderators_ids = array();
					if($event_moderators){
						$event_moderators_ids = explode(",", $event_moderators);
					}
					
					$merge_array = array_merge($admin_user_ids, $event_moderators_ids);
					$management_members = implode(",", $merge_array);					
					
					$statment = "AND text_by_user_id IN ('{$management_members}')";
				}
			}
			
			if(!is_user_logged_in()){
				$private_statment = "AND text_type IN ('public')";
			}else{
				$private_statment = "AND text_type IN ('public', 'private')";
			}
			
			$query = $wpdb->get_results("SELECT * FROM ". $table_prefix ."weca_chat_log WHERE text_sticky IS NULL AND weca_event_id = '{$sac_eventID}' AND text_id > ". $sac_lastID ." ".$statment." ".$private_statment." ORDER BY text_id DESC", ARRAY_A);
			
			for ($row = 0; $row < 1; $row++) {
				
				if (isset($query[$row]) && !empty($query[$row]) && is_array($query[$row])) {
					
					while (list($key, $value) = each($query[$row])) {
						
						$id   = $query[$row]['text_id'];
						$time = $query[$row]['text_datetime'];
						if($query[$row]['text_by_user_id']){
							$user_info = get_userdata($query[$row]['text_by_user_id']);							
							$name = $user_info->data->user_login;
						}else{
							$name = $query[$row]['name'];
						}
						$text = $query[$row]['text_message'];
						$url  = $query[$row]['text_type'];
						
						$time = sac_time_since($time);
						
						$loop = $id .'---'. $name .'---'. $text .'---'. $time .' '. esc_html__('ago', 'simple-ajax-chat') .'---'. $url .'---';
						
						$mod_message = false;
						$admin_message = false;
						$make_bold = false;
						
						if($query[$row]['text_by_user_id']){
							if(in_array($query[$row]['text_by_user_id'], $event_moderators_ids)){
								$mod_message = true;
								$make_bold = true;
							}
							
							if(in_array($query[$row]['text_by_user_id'], $user_info->roles)){
								$admin_message = true;
								$make_bold = true;
							}
						}
						
						if($query[$row]['text_to_user_id'] > 0 && $query[$row]['text_to_user_id'] != get_current_user_id() && $is_admin == false && $is_mod == false){							
							$loop = null;
						}
					}
					
				}
				
			}
			
		}
		
	}
	
	echo $loop;
	
}
add_action('init', 'sac_getData');



// edit chats
function sac_shout_edit() {
	
	global $wpdb, $table_prefix;
	
	if (!current_user_can('manage_options')) return;
	
	if (isset($_GET['sac_edit']) && isset($_GET['sac_comment_id']) && is_numeric($_GET['sac_comment_id'])) {
		
		$comment_id = $_GET['sac_comment_id'];
		
		$comment_text = isset($_GET['sac_text']) ? stripslashes($_GET['sac_text']) : '';
		
		$wpdb->query($wpdb->prepare("UPDATE ". $table_prefix ."weca_chat_log SET text = %s WHERE id = %d", $comment_text, $comment_id));
		
		$url = admin_url('options-general.php?page=simple_ajax_chat');
		$query = array('sac_edit' => 'true');
		$redirect = add_query_arg($query, $url);
		wp_redirect(esc_url_raw($redirect));
		
	}
	
}
add_action('admin_init', 'sac_shout_edit');



// delete chats
function sac_shout_delete() {
	
	global $wpdb, $table_prefix;
	
	if (!current_user_can('manage_options')) return;
	
	if (isset($_GET['sac_delete']) && isset($_GET['sac_comment_id']) && is_numeric($_GET['sac_comment_id'])) {
		
		$comment_id = $_GET['sac_comment_id'];
		
		$wpdb->query($wpdb->prepare("DELETE FROM ". $table_prefix ."weca_chat_log WHERE id = %d", $comment_id));
		
		$url = admin_url('options-general.php?page=simple_ajax_chat');
		$query = array('sac_delete' => 'true');
		$redirect = add_query_arg($query, $url);
		wp_redirect(esc_url_raw($redirect));
		
	}
	
}
add_action('admin_init', 'sac_shout_delete');



// truncate chats
function sac_shout_truncate() {
	
	global $wpdb, $table_prefix, $sac_options;
	
	if (!current_user_can('manage_options')) return;
	
	if (isset($_GET['sac_truncate'])) {
		
		$ip = sac_get_ip_address();
		$default_message = $sac_options['sac_default_message'];
		$default_handle  = $sac_options['sac_default_handle'];
		$sac_script_url  = $sac_options['sac_script_url'];
		if ($sac_script_url === '') $sac_script_url = site_url();
		
		$wpdb->query('TRUNCATE TABLE '. $table_prefix .'weca_chat_log');
		
		$url = admin_url('options-general.php?page=simple_ajax_chat');
		$query = array('sac_truncate_success' => 'true');
		$redirect = add_query_arg($query, $url);
		wp_redirect(esc_url_raw($redirect));
		
	}
	
}
add_action('admin_init', 'sac_shout_truncate');



// insert data
function sac_addData($sac_user_name, $sac_user_text, $sac_user_url, $weca_event_id, $text_type, $text_to_user_id = 0) {
	global $wpdb, $table_prefix, $sac_options;
	
	$ip = sac_get_ip_address();
	
	$sac_user_text = substr(trim($sac_user_text), 0, $sac_options['max_chars']);
	$sac_user_text = (empty($sac_user_text)) ? '' : sac_special_chars($sac_user_text);
	
	$sac_user_name = substr(trim($sac_user_name), 0, $sac_options['max_uname']);
	$sac_user_name = (empty($sac_user_name)) ? esc_html__('Anonymous', 'simple-ajax-chat') : sanitize_text_field($sac_user_name);
	
	$use_username  = $sac_options['sac_logged_name'];
	$current_user  = wp_get_current_user();
	$logged_name   = sanitize_text_field($current_user->display_name);
	
	if ($use_username && !empty($logged_name)) $sac_user_name = $logged_name;
	
	if (empty($sac_user_url) || $sac_user_url == 'http://' || $sac_user_url == 'https://') {
		
		$sac_user_url = '';
		
	} else {
		
		$sac_user_url = esc_url($sac_user_url);
		
	}
	
	$sac_censors = get_option('sac_censors', sac_default_censors());
	
	$censors    = explode(',', strval($sac_censors));
	$censors    = array_map('trim', $censors);
	$censored   = apply_filters('sac_censor_replace', '');
	$filter_url = apply_filters('sac_filter_user_url', false);
	
	if (!empty($censors)) {
		
		foreach ($censors as $censor) {
			
			if (stristr($sac_user_text, $censor)) {
				
				$sac_user_text = str_ireplace($censor, $censored, $sac_user_text);
				
			}
			
			if (stristr($sac_user_name, $censor)) {
				
				$sac_user_name = str_ireplace($censor, $censored, $sac_user_name);
				
			}
			
			if (stristr($sac_user_url, $censor) && $filter_url) {
				
				$sac_user_url = str_ireplace($censor, $censored, $sac_user_url);
				
			}
			
		}
		
	}
	
	$text_by_user_id = 0;
	if(is_user_logged_in()){
		$text_by_user_id = get_current_user_id();
	}
	
	$data = array(
		
		'text_datetime' => current_time('timestamp'), 
		'name' => stripslashes($sac_user_name), 
		'text_by_user_id' => $text_by_user_id, 
		'text_to_user_id' => $text_to_user_id,
		'text_message' => stripslashes($sac_user_text), 
		'ip'   => $ip,
		'weca_event_id' => $weca_event_id,
		'text_type' => $text_type
	);
	
	$wpdb->insert($table_prefix .'weca_chat_log', $data);
}



// clean up database
function sac_deleteOld() {
	
	global $wpdb, $table_prefix, $sac_options;
	
	$a = intval($wpdb->insert_id);
	$b = intval($sac_options['max_chats']);
	
	if (($a - $b) > $b) {
		
		$c = intval($a - $b);
		
		$wpdb->query($wpdb->prepare("DELETE FROM ". $table_prefix ."weca_chat_log WHERE id < %d", $c));
		
	}
	
}

// cron sixty minute interval
function sac_cron_sixty_minutes($schedules) {
	$schedules['sixty_minutes'] = array('interval' => 3600, 'display' => esc_html__('Sixty minutes'));
	return $schedules;
}
add_filter('cron_schedules', 'sac_cron_sixty_minutes');



// cron thirty minute interval
function sac_cron_thirty_minutes($schedules) {
	$schedules['thirty_minutes'] = array('interval' => 1800, 'display' => esc_html__('Thirty minutes'));
	return $schedules;
}
add_filter('cron_schedules', 'sac_cron_thirty_minutes');



// cron three minute interval
function sac_cron_three_minutes($schedules) {
	$schedules['three_minutes'] = array('interval' => 180, 'display' => esc_html__('Three minutes'));
	return $schedules;
}
add_filter('cron_schedules', 'sac_cron_three_minutes');



// cron thirty second interval
function sac_cron_thirty_seconds($schedules) {
	$schedules['thirty_seconds'] = array('interval' => 30, 'display' => esc_html__('Thirty Seconds'));
	return $schedules;
}
add_filter('cron_schedules', 'sac_cron_thirty_seconds');



// cron activate truncate
function sac_cron_activation() {
	if (!wp_next_scheduled('sac_cron_truncate')) {
		$interval = apply_filters('sac_truncate_chats_interval_filter', null);
		wp_schedule_event(time(), $interval, 'sac_cron_truncate'); // hourly, daily, twicedaily, thirty_minutes, three_minutes, thirty_seconds
	}
}
register_activation_hook(__FILE__, 'sac_cron_activation');



// cron deactivate truncate
function sac_cron_deactivation() {
	wp_clear_scheduled_hook('sac_cron_truncate');
}
register_deactivation_hook(__FILE__, 'sac_cron_deactivation');



// cron truncate chats
function sac_cron_truncate_chats() {
	
	global $wpdb, $table_prefix, $sac_options;
	
	if (!defined('DOING_CRON')) return;
	
	$default_message = isset($sac_options['sac_default_message']) ? $sac_options['sac_default_message'] : esc_html__('Welcome to the Chat Forum', 'simple-ajax-chat');
	$default_handle  = isset($sac_options['sac_default_handle'])  ? $sac_options['sac_default_handle']  : esc_html__('Simple Ajax Chat', 'simple-ajax-chat');
	
	$time = current_time('mysql');
	
	$truncate = $wpdb->query('TRUNCATE TABLE '. $table_prefix .'weca_chat_log');
	
	$insert = 0;
	if ($truncate) {
		$insert = $wpdb->insert($table_prefix .'weca_chat_log', array(
			
			'time' => current_time('timestamp'), 
			'name' => $default_handle, 
			'text' => $default_message, 
			'url'  => get_home_url(), 
			'ip'   => sac_get_ip_address()
					
		), array('%s', '%s', '%s', '%s', '%s'));
	}
	
	$truncate_result = esc_html__('Successfully truncated SAC table.', 'simple-ajax-chat');
	if ($truncate === false) $truncate_result = esc_html__('Unable to truncate SAC table.', 'simple-ajax-chat');
	elseif ($truncate === 0) $truncate_result = esc_html__('Truncate not needed, zero rows affected.', 'simple-ajax-chat');
	
	$insert_result = esc_html__('Successfully inserted default message.', 'simple-ajax-chat');
	if ($insert === false) $insert_result = esc_html__('Unable to insert default message.', 'simple-ajax-chat');
	elseif ($insert === 0) $insert_result = esc_html__('Default message not inserted.', 'simple-ajax-chat');
	
	do_action('sac_truncate_chats_action', $time, $truncate_result, $insert_result);
	
}
add_action('sac_cron_truncate', 'sac_cron_truncate_chats');



// display settings link on plugin page
function sac_plugin_action_links($links) {
	
	$href = admin_url('options-general.php?page=simple_ajax_chat');
	$link = '<a href="'. $href .'" title="'. esc_attr__('Visit SAC Settings', 'simple-ajax-chat') .'">'. esc_html__('Settings', 'simple-ajax-chat') .'</a>';
	
	return array_merge(array('settings'=> $link), $links);

}
add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'sac_plugin_action_links');



// rate plugin link
function add_sac_links($links, $file) {
	
	if ($file == plugin_basename(__FILE__)) {
		
		$href  = 'https://wordpress.org/support/plugin/simple-ajax-chat/reviews/?rate=5#new-post';
		$title = esc_attr__('Give us a 5-star rating at WordPress.org', 'simple-ajax-chat');
		$text  = esc_html__('Rate this plugin', 'simple-ajax-chat') .'&nbsp;&raquo;';
		
		$links[] = '<a target="_blank" href="'. $href .'" title="'. $title .'">'. $text .'</a>';
		
	}
	
	return $links;
	
}
add_filter('plugin_row_meta', 'add_sac_links', 10, 2);



// enqueue scripts frontend
function sac_enqueue_scripts() {
	
	global $sac_version, $sac_options;
	
	$protocol = is_ssl() ? 'https://' : 'http://';
	
	$script_url = isset($sac_options['sac_script_url']) ? $sac_options['sac_script_url'] : '';
	
	$current_url = esc_url_raw($protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
	
	$resource_url = plugin_dir_url(__FILE__) .'resources/custom.php';
	
	if (!empty($script_url)) {
		
		$script_urls = explode(',', $script_url);
		
		foreach ($script_urls as $url) {
			
			$url = esc_url_raw(trim($url));
			
			if (strpos($current_url, $url) !== false) {
				
				// wp_register_script($handle, $file, $deps, $ver, $footer);
				
				wp_register_script('sac', $resource_url, array('jquery'), null, true);
				wp_enqueue_script('sac');
				
			}
			
		}
		
	} else {
		
		wp_register_script('sac', $resource_url, array('jquery'), null, true);
		wp_enqueue_script('sac');
		
	}
	
}
add_action('wp_enqueue_scripts', 'sac_enqueue_scripts');



// sac shortcode
function sac_happens($arg) {
	
	if($arg['event_id']){
		ob_start();
		event_ajax_chat($arg['event_id'], $arg['is_mod'], $arg['is_admin'], $arg['event_moderators'], $arg['all_users']);
		$sac_happens = ob_get_contents();
		ob_end_clean();
	}
	
	return $sac_happens;
	
}
add_shortcode('sac_happens','sac_happens');



// replace characters
function sac_special_chars($s) {
	
	$s = wp_strip_all_tags($s, true);
	$s = sanitize_text_field($s);
	$s = str_replace('---', '&minus;-&minus;', $s);
	
	return $s;
	
}



// get IP address
function sac_get_ip_address() {
	
	if (isset($_SERVER)) {
		
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			
			$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			
		} elseif(isset($_SERVER['HTTP_CLIENT_IP'])) {
			
			$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			
		} else {
			
			$ip_address = $_SERVER['REMOTE_ADDR'];
			
		}
		
	} else {
		
		if(getenv('HTTP_X_FORWARDED_FOR')) {
			
			$ip_address = getenv('HTTP_X_FORWARDED_FOR');
			
		} elseif(getenv('HTTP_CLIENT_IP')) {
			
			$ip_address = getenv('HTTP_CLIENT_IP');
			
		} else {
			
			$ip_address = getenv('REMOTE_ADDR');
			
		}
		
	}
	
	return sanitize_text_field($ip_address);
	
}



// time since entry
function sac_time_since($original) {
	
	$chunks = array( // unix time (seconds)
		array(60 * 60 * 24 * 365 , esc_html__('year',   'simple-ajax-chat')), 
		array(60 * 60 * 24 * 30 ,  esc_html__('month',  'simple-ajax-chat')), 
		array(60 * 60 * 24 * 7,    esc_html__('week',   'simple-ajax-chat')), 
		array(60 * 60 * 24 ,       esc_html__('day',    'simple-ajax-chat')), 
		array(60 * 60 ,            esc_html__('hour',   'simple-ajax-chat')), 
		array(60 ,                 esc_html__('minute', 'simple-ajax-chat')), 
	);
	
	$original = $original - 10; // fixes bug where $time & $original match
	$today = current_time('timestamp'); // current unix time
	$since = $today - $original;
	
	for ($i = 0, $j = count($chunks); $i < $j; $i++) {
		
		$seconds = $chunks[$i][0];
		$name    = $chunks[$i][1];
		
		if (($count = floor($since / $seconds)) != 0) {
			
			break;
			
		}
		
	}
	
	$print = ($count == 1) ? '1 ' . $name : "$count {$name}" . esc_html__('s', 'simple-ajax-chat');
	
	if ($i + 1 < $j) {
		
		$seconds2 = $chunks[$i + 1][0];
		$name2    = $chunks[$i + 1][1];
		
		if (($count2 = floor(($since - ($seconds * $count)) / $seconds2)) != 0) {
			
			$print .= ($count2 == 1) ? ', 1 ' . $name2 : ", $count2 {$name2}" . esc_html__('s', 'simple-ajax-chat');
			
		}
		
	}
	
	return $print;
	
}



// prevent caching
if ($sacGetChat === 'yes' || $sacSendChat === 'yes') {
	
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
	header("Last-Modified: ". gmdate("D, d M Y H:i:s") ."GMT"); 
	header("Cache-Control: no-cache, must-revalidate"); 
	header("Pragma: no-cache");
	header("Content-Type: text/html; charset=utf-8");
	
	if (!$sac_lastID) $sac_lastID = 0;
	
}


