<?php
global $wpdb;
$table_prefix = $wpdb->prefix;

$wpdb->query("
CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."weca_chat_log` (
 `text_id` int(11) NOT NULL AUTO_INCREMENT,
 `weca_event_id` int(11) NOT NULL,
 `text_datetime` bigint(11) NOT NULL DEFAULT '0',
 `name` varchar(255) NOT NULL,
 `text_message` text NOT NULL,
 `ip` text NOT NULL,
 `text_room_status` varchar(255) NOT NULL,
 `text_by_user_id` int(11) NOT NULL,
 `text_to_user_id` bigint(20) NOT NULL,
 `text_type` varchar(255) NOT NULL,
 `text_sticky` text,
 `text_url` varchar(255) DEFAULT NULL,
 `text_promoted` varchar(255) DEFAULT NULL,
 `text_silenced` varchar(255) DEFAULT NULL,
 `text_sticky_in_date` date NOT NULL,
 `text_sticky_in_time` time NOT NULL,
 `text_sticky_out_date` date NOT NULL,
 `text_sticky_out_time` time NOT NULL,
 PRIMARY KEY (`text_id`),
 UNIQUE KEY `id` (`text_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1"
);

$wpdb->query("
CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."weca_analytics_user` (
 `weca_event_id` bigint(20) NOT NULL,
 `user_id` bigint(20) DEFAULT NULL,
 `user_email` text NOT NULL,
 `user_login_time` time NOT NULL,
 `user_login_delay` time NOT NULL,
 `user_page_out_time` time NOT NULL,
 `user_log_out_time` time NOT NULL,
 `user_page_out_missed` time NOT NULL,
 `user_log_out_missed` time NOT NULL,
 `user_event_page_duration` time NOT NULL,
 `user_other_pages` varchar(255) NOT NULL,
 `user_silence_time` time NOT NULL,
 `user_moderator` varchar(11) NOT NULL,
 `user_moderator_in_date` date NOT NULL,
 `user_moderator_in_time` time NOT NULL,
 `user_moderator_out_date` date NOT NULL,
 `user_moderator_out_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1"
);

$wpdb->query("
CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."weca_events` (
 `weca_event_id` int(11) NOT NULL AUTO_INCREMENT,
 `weca_event_title` varchar(255) NOT NULL,
 `weca_event_start_date` date NOT NULL,
 `weca_event_start_time` time NOT NULL,
 `weca_event_time_zone` varchar(255) NOT NULL,
 `weca_event_duration` varchar(255) NOT NULL,
 `weca_event_end_date` date NOT NULL,
 `weca_event_end_time` time NOT NULL,
 `weca_event_type` varchar(255) NOT NULL,
 `weca_event_mod_email_1` varchar(255) NOT NULL,
 `weca_event_mod_email_2` varchar(255) NOT NULL,
 `weca_event_mod_email_3` varchar(255) NOT NULL,
 `weca_event_mod_email_4` varchar(255) NOT NULL,
 `weca_event_mod_email_5` varchar(255) NOT NULL,
 `weca_event_attendee_list` varchar(255) NOT NULL,
 `weca_event_page_id` int(11) NOT NULL,
 `weca_chat_room` varchar(255) NOT NULL,
 PRIMARY KEY (`weca_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1
");

$wpdb->query("
CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."weca_user_actions` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `user_id` int(11) DEFAULT NULL,
 `ip` varchar(255) DEFAULT NULL,
 `event_id` int(11) NOT NULL,
 `action` varchar(255) NOT NULL,
 PRIMARY KEY (`id`),
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1"
);

$wpdb->query("
CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."weca_click_log` (
 `click_id` int(11) NOT NULL AUTO_INCREMENT,
 `weca_event_id` int(11),
 `click_by_user_id` bigint(20) NOT NULL,
 `click_url` varchar(255) NOT NULL,
 
 PRIMARY KEY (`click_id`)

) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1"
);

$wpdb->query("CREATE TABLE `".$wpdb->prefix."weca_attendance_analytics` (
 `weca_event_id` bigint(20) NOT NULL AUTO_INCREMENT,
 `analytics_attendance_015` int(11) DEFAULT NULL,
 `analytics_attendance_030` int(11) DEFAULT NULL,
 `analytics_attendance_045` int(11) DEFAULT NULL,
 `analytics_attendance_060` int(11) DEFAULT NULL,
 `analytics_attendance_075` int(11) DEFAULT NULL,
 `analytics_attendance_090` int(11) DEFAULT NULL,
 `analytics_attendance_105` int(11) DEFAULT NULL,
 `analytics_attendance_120` int(11) DEFAULT NULL,
 `analytics_attendance_135` int(11) DEFAULT NULL,
 `analytics_attendance_150` int(11) DEFAULT NULL,
 `analytics_attendance_165` int(11) DEFAULT NULL,
 `analytics_attendance_180` int(11) DEFAULT NULL,
 `analytics_attendance_195` int(11) DEFAULT NULL,
 `analytics_attendance_210` int(11) DEFAULT NULL,
 `analytics_attendance_225` int(11) DEFAULT NULL,
 `analytics_attendance_250` int(11) DEFAULT NULL,
 `analytics_attendance_peak` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
 PRIMARY KEY (`weca_event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1");


