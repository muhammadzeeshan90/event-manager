<?php 

if ($current_date > $DateBegin && $current_date < $DateEnd){
   
if(is_user_logged_in()){?>
<div class="event-join-box">
    <?php $class = ($selected_event != null) ? 'event_selected' : '';?>
    <button class="attend_event <?php echo $class ?>" data-event="<?php echo $event->weca_event_id ?>" ><?php echo ($selected_event != null) ? 'Joined' : get_option('placeholder_join_event')?> </button>
</div>
<?php } 

if($selected_event != null || $is_admin){?>
<div class="event_details">
	<ul>
		<li><span><?php echo get_option('placeholder_start')?>:</span> <?php echo date("F, d Y", strtotime($event->weca_event_start_date)); ?> - <?php echo date("h:i A", strtotime($event->weca_event_start_time)); ?></li>
		<li><span><?php echo get_option('placeholder_end')?>:</span> <?php echo date("F, d Y", strtotime($event->weca_event_end_date)); ?> - <?php echo date("h:i A", strtotime($event->weca_event_end_time)); ?></li>
		<li><span><?php echo get_option('placeholder_timezone')?>:</span> <?php echo $event->weca_event_time_zone; ?></li>		
	</ul>
</div>



<h3><?php echo get_option('placeholder_sticky_messages')?></h3>
<hr>
<div class="event-mod-output">
	<ul class="event-mod-comments">
	<?php
	 foreach(array_reverse($mod_comments) as $mod_comment) { ?>
		<li>
			<span><?php echo $mod_comment->username?>:</span>
			<?php echo $mod_comment->message?> 
		</li>
	<?php } ?>
	</ul>        
</div>
     
<?php
	
	if($event->weca_event_type == "private" && !is_user_logged_in()){
		return false;
	}
	
	echo "<hr><h3>".get_option('placeholder_chat_room')."</h3>";
	echo sac_happens(array("event_id" => $event->weca_event_id, "is_mod" => $is_mod, "is_admin" => $is_admin, "event_moderators" => $mod_ids, "all_users" => $all_users));
?>

<div class="attendee_list">
	<hr>
    <h3><?php echo get_option('placeholder_attendee_list')?></h3>
    <ul>
        <p>Total <?php echo sizeof($attendee_list)." Attendees"?></p>
        <?php

        if($event->weca_event_attendee_list == "visible" || $is_admin || $is_mod){
            foreach($attendee_list as $attendee){ ?>
                <li>
                    <?php 
                    echo $attendee->username;
                    
                    ?></li>
            <?php
            }
        }?>
    </ul>
</div>

<div class="event-mod-box">
    <?php
    if($is_admin || $is_mod){?>
	<hr>
	<h3>Mod Box</h3>
    <div class="event-chat-status-changed">
        <span>Chat Room Status: </span><span id="changed_chat_room_status"><?php echo $event->weca_chat_room;?></span>
        <form method="post">
            <input id="event_id_for_status" name="event_id_for_status" type="hidden" value="<?php echo $event->weca_event_id;?>">
            <input id="event_chat_status" name="event_chat_status" type="hidden" value="<?php echo $event->weca_chat_room;?>">
        <?php
        if($event->weca_chat_room == "open"){?>
            <input class="event_set_room_status" type="button" value="Set chat room to close">
        <?php
        }
        else{?>
            <input class="event_set_room_status" type="button" value="Set chat room to open">
        <?php
        }?>
        </form>
        
        
    </div>
    <div class="event-mod-text">
       
            <fieldset class="mod-fieldset">
                <label for="mod-text">Sticky Message:</label><br />
                <textarea id="mod-chat-message" name="mod_chat" placeholder="Sticky Message" rows="6" style="width: 100%;"></textarea>
            </fieldset>
            <div class="chat-submit">
                <button data-event="<?php echo $event->weca_event_id ?>" id="mod-chat-submit">Add Sticky Message</button>
            </div>
            
      
    </div>
	
	<div class="event-mod-text">
		<?php
			echo "<h4>Ban Users</h4>";
			$ban_users = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."weca_user_actions WHERE action = 'ban' AND event_id = '".$event->weca_event_id."' ORDER BY id DESC");
			echo "<ul>";
			if($ban_users){
				foreach($ban_users as $ban_user){
					if($ban_user->user_id > 0){
						$ban_user_info = get_userdata($ban_user->user_id);
						$baned = $ban_user_info->data->display_name;
					}else{
						$baned = $ban_user->ip;
					}
					echo '<li>'.$baned.'</li>';
				}
			}else{
				echo '<li>No Ban Users</li>';
			}
			echo "</ul>";
			
			echo "<h4>Silence Users</h4>";
			$silence_users = $wpdb->get_results("SELECT * FROM ". $wpdb->prefix ."weca_user_actions WHERE action = 'silence' AND event_id = '".$event->weca_event_id."' ORDER BY id DESC");
			echo "<ul>";
			if($silence_users){
				foreach($silence_users as $silence_user){
					if($silence_user->user_id > 0){
						$silence_user_info = get_userdata($silence_user->user_id);
						$silenced = $silence_user_info->data->display_name;
					}else{
						$silenced = $silence_user->ip;
					}
					echo '<li>'.$silenced.'</li>';
				}
			}else{
				echo '<li>No Silence Users</li>';
			}
			echo "</ul>";
		?>
	</div>
    <?php } ?> 
</div>
<?php }
    }else{
	$withcomments = "1";
	comments_template();
} ?>
