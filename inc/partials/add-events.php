<?php
foreach($mod_arr as $mod){?>
<div class="is-a-error">
    Moderator <?php echo $mod?> is not a registered user!
</div>
<?php } ?>
<div>
    <h1>Add Events</h1>
    <p>* Required Fields</p>
    <form method="post">
       <table class="form-table">
            <tr>
                <th>Event Title*</th>

                <td><input type='text' name='event_title' value='<?php echo $event->weca_event_title ?>' required/></td>
            </tr>
            <tr>
                <th>Event Start Date*</th>

                <td><input type='date' name='event_start_date' value='<?php echo $event->weca_event_start_date ?>' required/></td>
            </tr>
            <tr>
                <th>Event Start Time*</th>

                <td><input type='time' name='event_start_time' value='<?php echo $event->weca_event_start_time ?>' required/></td>
            </tr>
            <tr>
                <th>Event Time Zone*</th>

                <td>
					<select name='event_time_zone' required>
						<?php
						foreach($timezones as $name => $value){
							$selected = "";
							if($event->weca_event_time_zone == $value){
								$selected = 'selected="selected"';
							}
							echo '<option '.$selected.' value="'.$value.'">'.$name.'</option>';
						}
						?>
					</select>
				</td>
            </tr>
            <tr>
                <th>Event Duration*</th>

                <td><input type='number' min=0 name='event_duration_days' value='<?php echo $duration_days ?>' required/> Days
                    <input type='number' min=0 max=23 name='event_duration_hours' value='<?php echo $duration_hours ?>' required/> Hours
                    <input type='number' min=0 max=59 name='event_duration_mins' value='<?php echo $duration_mins ?>' required/> Minutes
                </td>
            </tr>
            <tr>
                <th>Event Type*</th>

                <td>
                    <select name="event_type">
                      <option value="public" <?php echo $public ;?>>Public</option>
                      <option value="private" <?php echo $private;?>>Private</option>
                    </select>
                </td>
            </tr>
               <tr>
                <th>Event Moderator Email 1*</th>

                <td><input type='email' name='event_email_1' value='<?php echo $event->weca_event_mod_email_1 ?>' required/></td>
            </tr>
            <tr>
                <th>Event Moderator Email 2</th>

                <td><input type='email' name='event_email_2' value='<?php echo $event->weca_event_mod_email_2?>'/></td>
            </tr>
            <tr>
                <th>Event Moderator Email 3</th>

                <td><input type='email' name='event_email_3' value='<?php echo $event->weca_event_mod_email_3?>'/></td>
            </tr>
            <tr>
                <th>Event Moderator Email 4</th>

                <td><input type='email' name='event_email_4' value='<?php echo $event->weca_event_mod_email_4?>'/></td>
            </tr>
            <tr>
                <th>Event Moderator Email 5</th>

                <td><input type='email' name='event_email_5' value='<?php echo $event->weca_event_mod_email_5?>'/></td>
            </tr>
            <tr>
                <th>Attendee List*</th>
                <td>
                    <select name="attendee_list">
                      <option value="visible" <?php echo $visible?>>Visible</option>
                      <option value="invisible"  <?php echo $invisible?>>Invisible</option>
                    </select>
                </td>
            </tr>
			<tr>
                <th>Chat Room*</th>
                <td>
                    <select name="chat_room" required>
					<?php
					if($event->weca_chat_room == "open"){
					?>
						<option value="open" selected>Open</option>
						<option value="close">Close</option>
					<?php
					}else{
						?>
						<option value="open">Open</option>
						<option value="close" selected>Close</option>
					<?php
					}
					?>                      
                    </select>
                </td>
            </tr>
        </table>
        <?php 
        if($_GET['action']=='edit'){?>
           <input type="submit" value="Update Event" <?php echo $disable ?>>
        <?php
        }
        else{
            submit_button('Add Event', 'primary'); 
        }
        ?>
    </form>
   
</div>