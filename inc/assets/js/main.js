jQuery(function () {
    
   jQuery( window ).on('unload', function() {
  
       
        var data = {
            action: 'on_browser_close'
             
        };

        jQuery.post(event_ajax.ajax_url, data, function (data) {
            
        });
    });
    
    jQuery('a').on('click', function(){
        var href = jQuery(this).attr('href');
        var data = {
            action: 'on_click_url',
            href: href
             
        };

        jQuery.post(event_ajax.ajax_url, data, function (data) {
            console.log(data);
        });
       
    });
	
	jQuery('.custom_tabs li').click(function(){
		$this = jQuery(this);
		$contentCont = $this.attr('data-tab');
		
		jQuery(".custom_tabs>li.active").removeClass("active");
		jQuery('#sac-output>ul').hide();
		
		$this.addClass('active');
		jQuery('#'+$contentCont).show();
	});
	
    var adding_attendee = false;
	jQuery('.sac-chat-message').click(function(){
		jQuery('.text_admin_menu').hide();
		jQuery(this).next('.text_admin_menu').show();
	});
	
    jQuery(".attend_event").on("click", function(){
        
        if(!jQuery(this).hasClass('event_selected') && !adding_attendee){
            
            adding_attendee = true;
            var data = {
                action: 'add_attendee_to_event',
                event_id: jQuery(this).attr('data-event')

            };

            jQuery.post(event_ajax.ajax_url, data, function (data) {
//               jQuery(".attend_event").addClass('event_selected');
//                jQuery(".attend_event.event_selected").html("Joined");
//                jQuery(".attendee_list ul p").html("Total " + (parseInt(jQuery(".attendee_list ul p").html().split(" ")[1]) + 1) + " Attendees");
//                var response = JSON.parse(data);             
//                if(response.resp){
//                    jQuery(".attendee_list ul").append("<li>"+response.user+"</li>");
//                }
                adding_attendee = false;
                location.reload();
           

            });
        }
    });
    
   
    
    jQuery("#mod-chat-submit").on("click", function(){
        var message = jQuery("#mod-chat-message").val();
        if(message != '' && message != null && !(/^\s*$/.test(message))){
          
            var data = {
                action: 'add_sticky_message',
                message: jQuery.trim(message),
                event_id: jQuery(this).attr('data-event')

            };
        
            jQuery.post(event_ajax.ajax_url, data, function (resp) {
                var html = "<li><span>"+resp+":</span>"+message+"</li>";
                jQuery(".event-mod-comments").prepend(html);
                
                jQuery("#mod-chat-message").val('');


            });
        }
        jQuery("#mod-chat-submit").blur();
        
        
        
        
    });
    
    jQuery(".event_set_room_status").on("click", function(){
        $chat_status_before = jQuery("#event_chat_status").val();
        var data = {
            action: 'set_room_status',
            event_id: jQuery("#event_id_for_status").val(),
            status: jQuery("#event_chat_status").val()

        };
        jQuery.post(event_ajax.ajax_url, data, function (resp) {
           
            jQuery("#changed_chat_room_status").html(resp);
            jQuery(".event_set_room_status").val("Set chat room to "+$chat_status_before);
            jQuery("#event_chat_status").val(resp);

        });
        
    });
    
    jQuery(".silence-user").on("click", function(){
         var data = {
            action: 'silence_user',
            event_id: jQuery(this).attr('data-event'),
            user_id: jQuery(this).attr('data-user')

        };

        jQuery.post(event_ajax.ajax_url, data, function (resp) {
           


        });
    })

})

function checkForChanges(eventID, chatRoom, is_mod, is_admin){
    var data = {
		action: 'check_for_changes',
		event_id: eventID,
		total_comments: jQuery(".event-mod-comments li").length,
		chat_room: chatRoom

	};
	
    jQuery.post(event_ajax.ajax_url, data, function (resp) {  
        var response = JSON.parse(resp); 
		if(response.chatroom_status != jQuery('#current_room_status').val()){
			if(is_mod == false && is_admin == false){
				if(response.chatroom_status == "open"){
					jQuery(".chat_room_closed_message").hide();
					jQuery("#sac-form").show();			
				}else{
					jQuery("#sac-form").hide();
					jQuery(".chat_room_closed_message").show();				
				}
			}
			
			jQuery('#current_room_status').val(response.chatroom_status);
		}
		
		if(response.refresh){
			jQuery(".event-mod-comments").html(response.html);
		}
		
		checkForChanges(eventID, chatRoom, is_mod, is_admin);
	});
    
}
       