<?php


class Event{
    
    public function __construct() {
        add_action('get_header', array($this, 'check_for_page_id'));
        add_action('admin_init', array($this, 'register_scripts')); 
        add_action('wp_enqueue_scripts', array($this, 'register_scripts'));       
        add_action('admin_menu', array($this, 'add_settings_page'));
        add_shortcode('event_page', array($this, 'event_page'));
        add_shortcode('testcron', array($this, 'testcron'));
        add_action('wp_ajax_add_attendee_to_event', array($this, 'add_attendee_to_events'));
        add_action('wp_ajax_add_sticky_message', array($this, 'add_sticky_message'));
        add_action('wp_ajax_set_room_status', array($this, 'set_room_status'));
        add_action('wp_ajax_silence_user', array($this, 'silence_user_callback'));
        add_action('wp_ajax_on_browser_close', array($this, 'on_browser_close'));
        add_action('wp_ajax_nopriv_silence_user', array($this, 'silence_user_callback'));
        add_action('wp_ajax_check_for_changes', array($this, 'check_for_changes'));
        add_action('wp_ajax_nopriv_check_for_changes', array($this, 'check_for_changes'));
        add_action('wp_ajax_on_click_url', array($this, 'on_click_url'));
        add_action('clear_auth_cookie', array($this,'after_logout'));
        add_action('wp_login', array($this,'after_login'));
    }
    
    
    public function check_for_page_id(){
        
        global $post;
        global $wpdb;
        $page_id = get_the_ID();
        $user = wp_get_current_user();
        if(is_user_logged_in()){
            if(($_COOKIE['event_id']) != $page_id){
                
                if($_COOKIE['event_id']){
                    $event = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_events WHERE weca_event_page_id= '.$_COOKIE['event_id']);

                    $event_end_time = new DateTime($event->weca_event_end_date.' '.$event->weca_event_end_time);
                    $current_date = new DateTime();

                    $page_out_missed = date_diff($current_date, $event_end_time);
                    $page_out_missed->format('%a %h %i %s');


                    if($page_out_missed->invert == 0){
                         $page_out = $page_out_missed->days.':'.$page_out_missed->h.':'.$page_out_missed->i.':'.$page_out_missed->s;
                    }
                    else{
                        $page_out = "00:00:00:00";
                    }
                    $data = array(
                        'user_page_out_missed' => $page_out,
                        'user_page_out_time'  => date('h:i:s')

                    );

                    $wpdb->update($wpdb->prefix."weca_analytics_user", $data,  array("weca_event_id"=>$event->weca_event_id, "user_id"=>$user->ID));
                    setcookie('event_id', $page_id, time()-1, "/");
                    delete_user_meta($user->ID, 'event_joined', $event->weca_event_id);

                }
                if(has_shortcode($post->post_content, 'event_page')){      
                    setcookie('event_id', $page_id, time() + (86400 * 30), "/");
                }
                
            }
            
            $user_other_events = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'weca_analytics_user WHERE user_id='.$user->ID);
            
            if(has_shortcode($post->post_content, 'event_page')){      
                
                $event = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_events WHERE weca_event_page_id= '.$page_id);
                $user_other_events = $wpdb->get_results('SELECT * FROM '.$wpdb->prefix.'weca_analytics_user WHERE weca_event_id != '.$event->weca_event_id.' AND user_id='.$user->ID);
                
                
                
            }
           
            foreach($user_other_events as $user_other_event){
                $other_page = unserialize($user_other_event->user_other_pages);
                 
                $other_page[] = $post->post_title;
                $data = array(
                    'user_other_pages' => serialize($other_page)                    
                );
               
               
                $wpdb->update($wpdb->prefix."weca_analytics_user", $data,  array("weca_event_id"=>$user_other_event->weca_event_id, "user_id"=>$user->ID));

            }
            
        }
       
        
     
       
        
    }
    
    public function register_scripts() {
        wp_enqueue_script('main', plugins_url('/assets/js/main.js', __FILE__), array('jquery'));
       
        wp_localize_script('main', 'event_ajax', array('ajax_url' => admin_url('admin-ajax.php')));
        wp_enqueue_style('stylesheet', plugins_url('/assets/css/stylesheet.css', __FILE__));
    }

    
    public function add_settings_page(){
        add_menu_page('Webinar Press', 'Webinar Press', 'manage_options', 'events', array($this, 'all_event_page'), '', 25);
        add_submenu_page('events', 'All Events', 'All Events', 'manage_options', 'events', array($this, 'all_event_page'));
        add_submenu_page('events', 'Add Events', 'Add New', 'manage_options', 'add-events', array($this, 'add_event_page'));
        add_submenu_page('events', 'Placeholder Settings', 'Placeholder Settings', 'manage_options', 'placeholder-settings', array($this, 'add_placeholder_settings'));
    }
    
    public function add_placeholder_settings(){
        
        $settings = array(
            'name' => 'Enter Your Name',
            'email' => 'Enter Your Email',
            'join_event'=> 'Join Event',
            'sticky_messages'=> 'Sticky Messages',
            'start'=> 'Start',
            'end'=> 'End',
            'timezone'=> 'Timezone',
            'chat_room'=> 'Chat Room',
            'public messages'=> 'Public Messages',
            'private messages'=> 'Private Messages',
            'message'=> 'Message',
            'send'=> 'Send',
            'attendee_list'=> 'Attendee List'
            
        );
        if($_POST){
            foreach($_POST as $key=>$setting){                
                update_option('placeholder_'.$key, $setting);
               
            }
            
            
        }
        
        include(dirname(__FILE__) . DS . 'partials' . DS . 'placeholder-settings.php');
        
    }
    
    public function add_event_page(){
        global $wpdb;   
        $invisible = "selected";
        $visible =  "";
        $public = "selected";
        $private = "";
		
		$timezones = array (
			'(GMT-11:00) Midway Island' => 'Pacific/Midway',
			'(GMT-11:00) Samoa' => 'Pacific/Samoa',
			'(GMT-10:00) Hawaii' => 'Pacific/Honolulu',
			'(GMT-09:00) Alaska' => 'US/Alaska',
			'(GMT-08:00) Pacific Time (US &amp; Canada)' => 'America/Los_Angeles',
			'(GMT-08:00) Tijuana' => 'America/Tijuana',
			'(GMT-07:00) Arizona' => 'US/Arizona',
			'(GMT-07:00) Chihuahua' => 'America/Chihuahua',
			'(GMT-07:00) La Paz' => 'America/Chihuahua',
			'(GMT-07:00) Mazatlan' => 'America/Mazatlan',
			'(GMT-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain',
			'(GMT-06:00) Central America' => 'America/Managua',
			'(GMT-06:00) Central Time (US &amp; Canada)' => 'US/Central',
			'(GMT-06:00) Guadalajara' => 'America/Mexico_City',
			'(GMT-06:00) Mexico City' => 'America/Mexico_City',
			'(GMT-06:00) Monterrey' => 'America/Monterrey',
			'(GMT-06:00) Saskatchewan' => 'Canada/Saskatchewan',
			'(GMT-05:00) Bogota' => 'America/Bogota',
			'(GMT-05:00) Eastern Time (US &amp; Canada)' => 'US/Eastern',
			'(GMT-05:00) Indiana (East)' => 'US/East-Indiana',
			'(GMT-05:00) Lima' => 'America/Lima',
			'(GMT-05:00) Quito' => 'America/Bogota',
			'(GMT-04:00) Atlantic Time (Canada)' => 'Canada/Atlantic',
			'(GMT-04:30) Caracas' => 'America/Caracas',
			'(GMT-04:00) La Paz' => 'America/La_Paz',
			'(GMT-04:00) Santiago' => 'America/Santiago',
			'(GMT-03:30) Newfoundland' => 'Canada/Newfoundland',
			'(GMT-03:00) Brasilia' => 'America/Sao_Paulo',
			'(GMT-03:00) Buenos Aires' => 'America/Argentina/Buenos_Aires',
			'(GMT-03:00) Georgetown' => 'America/Argentina/Buenos_Aires',
			'(GMT-03:00) Greenland' => 'America/Godthab',
			'(GMT-02:00) Mid-Atlantic' => 'America/Noronha',
			'(GMT-01:00) Azores' => 'Atlantic/Azores',
			'(GMT-01:00) Cape Verde Is.' => 'Atlantic/Cape_Verde',
			'(GMT+00:00) Casablanca' => 'Africa/Casablanca',
			'(GMT+00:00) Edinburgh' => 'Europe/London',
			'(GMT+00:00) Greenwich Mean Time : Dublin' => 'Etc/Greenwich',
			'(GMT+00:00) Lisbon' => 'Europe/Lisbon',
			'(GMT+00:00) London' => 'Europe/London',
			'(GMT+00:00) Monrovia' => 'Africa/Monrovia',
			'(GMT+00:00) UTC' => 'UTC',
			'(GMT+01:00) Amsterdam' => 'Europe/Amsterdam',
			'(GMT+01:00) Belgrade' => 'Europe/Belgrade',
			'(GMT+01:00) Berlin' => 'Europe/Berlin',
			'(GMT+01:00) Bern' => 'Europe/Berlin',
			'(GMT+01:00) Bratislava' => 'Europe/Bratislava',
			'(GMT+01:00) Brussels' => 'Europe/Brussels',
			'(GMT+01:00) Budapest' => 'Europe/Budapest',
			'(GMT+01:00) Copenhagen' => 'Europe/Copenhagen',
			'(GMT+01:00) Ljubljana' => 'Europe/Ljubljana',
			'(GMT+01:00) Madrid' => 'Europe/Madrid',
			'(GMT+01:00) Paris' => 'Europe/Paris',
			'(GMT+01:00) Prague' => 'Europe/Prague',
			'(GMT+01:00) Rome' => 'Europe/Rome',
			'(GMT+01:00) Sarajevo' => 'Europe/Sarajevo',
			'(GMT+01:00) Skopje' => 'Europe/Skopje',
			'(GMT+01:00) Stockholm' => 'Europe/Stockholm',
			'(GMT+01:00) Vienna' => 'Europe/Vienna',
			'(GMT+01:00) Warsaw' => 'Europe/Warsaw',
			'(GMT+01:00) West Central Africa' => 'Africa/Lagos',
			'(GMT+01:00) Zagreb' => 'Europe/Zagreb',
			'(GMT+02:00) Athens' => 'Europe/Athens',
			'(GMT+02:00) Bucharest' => 'Europe/Bucharest',
			'(GMT+02:00) Cairo' => 'Africa/Cairo',
			'(GMT+02:00) Harare' => 'Africa/Harare',
			'(GMT+02:00) Helsinki' => 'Europe/Helsinki',
			'(GMT+02:00) Istanbul' => 'Europe/Istanbul',
			'(GMT+02:00) Jerusalem' => 'Asia/Jerusalem',
			'(GMT+02:00) Kyiv' => 'Europe/Helsinki',
			'(GMT+02:00) Pretoria' => 'Africa/Johannesburg',
			'(GMT+02:00) Riga' => 'Europe/Riga',
			'(GMT+02:00) Sofia' => 'Europe/Sofia',
			'(GMT+02:00) Tallinn' => 'Europe/Tallinn',
			'(GMT+02:00) Vilnius' => 'Europe/Vilnius',
			'(GMT+03:00) Baghdad' => 'Asia/Baghdad',
			'(GMT+03:00) Kuwait' => 'Asia/Kuwait',
			'(GMT+03:00) Minsk' => 'Europe/Minsk',
			'(GMT+03:00) Nairobi' => 'Africa/Nairobi',
			'(GMT+03:00) Riyadh' => 'Asia/Riyadh',
			'(GMT+03:00) Volgograd' => 'Europe/Volgograd',
			'(GMT+03:30) Tehran' => 'Asia/Tehran',
			'(GMT+04:00) Abu Dhabi' => 'Asia/Muscat',
			'(GMT+04:00) Baku' => 'Asia/Baku',
			'(GMT+04:00) Moscow' => 'Europe/Moscow',
			'(GMT+04:00) Muscat' => 'Asia/Muscat',
			'(GMT+04:00) St. Petersburg' => 'Europe/Moscow',
			'(GMT+04:00) Tbilisi' => 'Asia/Tbilisi',
			'(GMT+04:00) Yerevan' => 'Asia/Yerevan',
			'(GMT+04:30) Kabul' => 'Asia/Kabul',
			'(GMT+05:00) Islamabad' => 'Asia/Karachi',
			'(GMT+05:00) Karachi' => 'Asia/Karachi',
			'(GMT+05:00) Tashkent' => 'Asia/Tashkent',
			'(GMT+05:30) Chennai' => 'Asia/Calcutta',
			'(GMT+05:30) Kolkata' => 'Asia/Kolkata',
			'(GMT+05:30) Mumbai' => 'Asia/Calcutta',
			'(GMT+05:30) New Delhi' => 'Asia/Calcutta',
			'(GMT+05:30) Sri Jayawardenepura' => 'Asia/Calcutta',
			'(GMT+05:45) Kathmandu' => 'Asia/Katmandu',
			'(GMT+06:00) Almaty' => 'Asia/Almaty',
			'(GMT+06:00) Astana' => 'Asia/Dhaka',
			'(GMT+06:00) Dhaka' => 'Asia/Dhaka',
			'(GMT+06:00) Ekaterinburg' => 'Asia/Yekaterinburg',
			'(GMT+06:30) Rangoon' => 'Asia/Rangoon',
			'(GMT+07:00) Bangkok' => 'Asia/Bangkok',
			'(GMT+07:00) Hanoi' => 'Asia/Bangkok',
			'(GMT+07:00) Jakarta' => 'Asia/Jakarta',
			'(GMT+07:00) Novosibirsk' => 'Asia/Novosibirsk',
			'(GMT+08:00) Beijing' => 'Asia/Hong_Kong',
			'(GMT+08:00) Chongqing' => 'Asia/Chongqing',
			'(GMT+08:00) Hong Kong' => 'Asia/Hong_Kong',
			'(GMT+08:00) Krasnoyarsk' => 'Asia/Krasnoyarsk',
			'(GMT+08:00) Kuala Lumpur' => 'Asia/Kuala_Lumpur',
			'(GMT+08:00) Perth' => 'Australia/Perth',
			'(GMT+08:00) Singapore' => 'Asia/Singapore',
			'(GMT+08:00) Taipei' => 'Asia/Taipei',
			'(GMT+08:00) Ulaan Bataar' => 'Asia/Ulan_Bator',
			'(GMT+08:00) Urumqi' => 'Asia/Urumqi',
			'(GMT+09:00) Irkutsk' => 'Asia/Irkutsk',
			'(GMT+09:00) Osaka' => 'Asia/Tokyo',
			'(GMT+09:00) Sapporo' => 'Asia/Tokyo',
			'(GMT+09:00) Seoul' => 'Asia/Seoul',
			'(GMT+09:00) Tokyo' => 'Asia/Tokyo',
			'(GMT+09:30) Adelaide' => 'Australia/Adelaide',
			'(GMT+09:30) Darwin' => 'Australia/Darwin',
			'(GMT+10:00) Brisbane' => 'Australia/Brisbane',
			'(GMT+10:00) Canberra' => 'Australia/Canberra',
			'(GMT+10:00) Guam' => 'Pacific/Guam',
			'(GMT+10:00) Hobart' => 'Australia/Hobart',
			'(GMT+10:00) Melbourne' => 'Australia/Melbourne',
			'(GMT+10:00) Port Moresby' => 'Pacific/Port_Moresby',
			'(GMT+10:00) Sydney' => 'Australia/Sydney',
			'(GMT+10:00) Yakutsk' => 'Asia/Yakutsk',
			'(GMT+11:00) Vladivostok' => 'Asia/Vladivostok',
			'(GMT+12:00) Auckland' => 'Pacific/Auckland',
			'(GMT+12:00) Fiji' => 'Pacific/Fiji',
			'(GMT+12:00) International Date Line West' => 'Pacific/Kwajalein',
			'(GMT+12:00) Kamchatka' => 'Asia/Kamchatka',
			'(GMT+12:00) Magadan' => 'Asia/Magadan',
			'(GMT+12:00) Marshall Is.' => 'Pacific/Fiji',
			'(GMT+12:00) New Caledonia' => 'Asia/Magadan',
			'(GMT+12:00) Solomon Is.' => 'Asia/Magadan',
			'(GMT+12:00) Wellington' => 'Pacific/Auckland',
			'(GMT+13:00) Nuku\'alofa' => 'Pacific/Tongatapu'
			);

         $disable = "";
        if($_GET['action']=='edit'){
           
            $event = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_events WHERE weca_event_id='.$_GET['event']);
            if($event->weca_event_attendee_list == 'visible'){
                $visible =  "selected";
                $invisible = "";
            }
            if($event->weca_event_type == 'private'){
                $private =  "selected";
                $public = "";
            }
            
            $duration_days = floor($event->weca_event_duration / 60 / 24);
            $duration_hours = floor(($event->weca_event_duration - (24 * $duration_days*60))/60);
            $duration_mins = $event->weca_event_duration - (24 * $duration_days * 60) - ($duration_hours*60);
            $current_date = strtotime(date("Y-m-d H:i:s"));               
            $DateEnd = strtotime($event->weca_event_end_date.' '.$event->weca_event_end_time);
            if($DateEnd < $current_date){
                $disable = "disabled";
            }
           
            
            
        }
        $mod_arr = array();
        if(!empty($_POST['event_title']) && !empty($_POST['event_start_date']) && !empty($_POST['event_start_time']) && !empty($_POST['event_time_zone']) && !empty($_POST['event_type']) && !empty($_POST['event_email_1'])  && !empty($_POST['attendee_list']) ){
            $mod_arr = array();
            for($i=1;$i<5;$i++){
                if(!empty($_POST['event_email_'.$i])){
                    if(!get_user_by('email', $_POST['event_email_'.$i])){                     
                        $mod_arr[] =  $i;
                    }
    
                }
                

            }
            if(sizeof($mod_arr) == 0){
               
                $days = $_POST['event_duration_days'];
                $hours = $_POST['event_duration_hours'];
                $mins = $_POST['event_duration_mins'];
                $total_minutes = $this->get_total_minutes($days, $hours, $mins);
                
                $start = strtotime($_POST['event_start_date'].' '.$_POST['event_start_time']);        
                $end = strftime ('%x %X', $start + ($total_minutes * 60));
                $endDate = date('Y-m-d', strtotime($end));
                $end = explode(" ", $end);            
                $endTime = $end[1];
                $event_id = '';
                if($_GET['action']=='edit'){
                    $event_id = $event->weca_event_id;
                    $page_id = $event->weca_event_page_id;
                    $query_type = 'replace';
                    wp_update_post(array(
                        "ID" => $page_id,
                        "post_title" => $_POST['event_title']
                    ));
                }
                else{
                    $page_array = array(
                        'post_type' => 'page',
                        'post_status' => 'publish',
                        'post_title' => $_POST['event_title'],
                        'post_content' => '[event_page]',
                        'comment_status' => 'open'
                    );

                    $page_id = wp_insert_post($page_array);
                    $query_type = 'insert';
                

                }

                $data = array(
                    'weca_event_id' => $event_id,
                    'weca_event_title'=> $_POST['event_title'],
                    'weca_event_start_date' => $_POST['event_start_date'],
                    'weca_event_start_time' => $_POST['event_start_time'],
                    'weca_event_time_zone' => $_POST['event_time_zone'],
                    'weca_event_duration' =>$total_minutes,
                    'weca_event_type' => $_POST['event_type'],
                    'weca_event_end_date' => $endDate,
                    'weca_event_end_time' => $endTime,
                    'weca_event_mod_email_1' =>$_POST['event_email_1'],
                    'weca_event_mod_email_2' => $_POST['event_email_2'],
                    'weca_event_mod_email_3' => $_POST['event_email_3'],
                    'weca_event_mod_email_4' => $_POST['event_email_4'],
                    'weca_event_mod_email_5' => $_POST['event_email_5'],
                    'weca_event_attendee_list' => $_POST['attendee_list'],
                    'weca_event_page_id' => $page_id,
                    'weca_chat_room' => $_POST['chat_room'],
                );

                $wpdb->$query_type($wpdb->prefix.'weca_events', $data);   
                $current_date = strtotime(date("Y-m-d H:i:s"));               
                $DateEnd = strtotime($endDate.' '.$endTime);
                if($DateEnd < $current_date){
                    $disable = "disabled";
                }
                 
                
            }
            
            
            
        }
        
       
        include(dirname(__FILE__) . DS . 'partials' . DS . 'add-events.php');
    }
    public function all_event_page(){
        global $wpdb;
        if($_GET['action']=='delete'){
            $event = $wpdb->get_row('SELECT weca_event_page_id FROM '.$wpdb->weca_events.'weca_events WHERE weca_event_id='.$_GET['event']);
            $page_id = $event->weca_event_page_id;
            $wpdb->delete($wpdb->prefix.'weca_events', array( 'weca_event_id'=> $_GET['event'] ));           
            wp_delete_post($page_id, 'true');
            
        }
        $myListTable = new My_List_Table();        
        $myListTable->prepare_items(); 
        $myListTable->display(); 
         
    }
    
    public function set_room_status(){
        global $wpdb;
        $chat_status = "open";
        if($_POST['status'] == "open"){
            $chat_status = "close";
        }
        $data = array(
            "weca_chat_room" => $chat_status

        );
        $event_id= $_POST['event_id'];
        $wpdb->update($wpdb->prefix."weca_events", $data,  array("weca_event_id"=>$_POST['event_id']));
        echo $chat_status;
        wp_die();
        
    }
    
    public function event_page(){
        global $wpdb;  
       
        $page_id = get_the_ID();       
        $event_query = "SELECT * FROM ".$wpdb->prefix."weca_events WHERE weca_event_page_id = '".$page_id."';";
        $event = $wpdb->get_row($event_query); 
        
        $user = wp_get_current_user();
        $is_user_silenced_from_event = false;
        $key = "user_silence_from_".$event->weca_event_id;
        if(get_user_meta($user_id, $key, true) != ""){
            $is_user_silenced_from_event = true;
        }
        $selected_event_query = "SELECT * FROM ".$wpdb->prefix."weca_analytics_user WHERE weca_event_id = '".$event->weca_event_id."' AND user_id = '".$user->ID."';";
        $selected_event = $wpdb->get_row($selected_event_query);
        $attendee_list_query = "SELECT analytics.user_ID AS userid, user.user_nicename AS username FROM ".$wpdb->prefix."weca_analytics_user AS analytics LEFT JOIN wp_users AS user ON analytics.user_ID = user.ID WHERE analytics.weca_event_id = '".$event->weca_event_id."';"; 
        $attendee_list = $wpdb->get_results($attendee_list_query);
        
        $is_mod = false;
        $is_admin = current_user_can('administrator');
        $mod_ids = array();
		
        if(!$is_admin && is_user_logged_in()){
            for($i=0;$i<=5;$i++){
                $mod = 'weca_event_mod_email_'.$i;
				$moderator = get_user_by("email", $event->$mod);
				$mod_ids[] = $moderator->ID;
                if($user->user_email==$event->$mod){
					if($is_mod == false){
						$is_mod = true;
					}
                }

            }
        }
        
        

        if($event->weca_event_type == "private" && !is_user_logged_in()){
            $location = wp_login_url();
            echo "<meta http-equiv=’refresh’ content='0;url=$location' />";
        }
                
           
        $comment_query = "SELECT user.user_nicename AS username, chat.text_message AS message FROM ".$wpdb->prefix."weca_chat_log AS chat LEFT JOIN wp_users AS user ON user.ID = chat.text_by_user_id WHERE weca_event_id = '".$event->weca_event_id."' AND text_sticky='no'";
        $event_comments = $wpdb->get_results($comment_query);
        $mod_comment_query = "SELECT user.user_nicename AS username, chat.text_message AS message FROM ".$wpdb->prefix."weca_chat_log AS chat LEFT JOIN wp_users AS user ON user.ID = chat.text_by_user_id WHERE weca_event_id = '".$event->weca_event_id."' AND text_sticky='yes'";
        $mod_comments = $wpdb->get_results($mod_comment_query);
		
		$default_timezone = date_default_timezone_get();
		date_default_timezone_set($event->weca_event_time_zone);
		
        $current_date = strtotime(date("Y-m-d H:i:s"));
        $DateBegin = strtotime($event->weca_event_start_date.' '.$event->weca_event_start_time);
        $DateEnd = strtotime($event->weca_event_end_date.' '.$event->weca_event_end_time);
        $all_users = get_users(array("exclude" => array(get_current_user_id())));
		
        if($event->weca_event_type == "public" || is_user_logged_in()){
            include(dirname(__FILE__) . DS . 'partials' . DS . 'event-chat.php');
            echo '<script type="text/javascript">',
             'checkForChanges("'.$event->weca_event_id.'", "'.$event->weca_chat_room.'", "'.$is_mod.'", "'.$is_admin.'");',
             '</script>';
        }
        if(is_user_logged_in()){
            if ($current_date > $DateBegin && $current_date < $DateEnd){
                $events_joined_by_user =  get_user_meta($user->ID, 'event_joined');               
                if(!in_array($event->weca_event_id, $events_joined_by_user)){
                     add_user_meta($user->ID, 'event_joined', $event->weca_event_id);
                    
                }
                   
               
            }
        }
       
        
		date_default_timezone_set($default_timezone);
        
       
    }
    
    public function add_attendee_to_events($user_id ){
        global $wpdb;
        $event_id = $_POST['event_id'];
        $event_query = "SELECT * FROM ".$wpdb->prefix."weca_events WHERE weca_event_id = '".$event_id."';";
        $event = $wpdb->get_row($event_query);
        $user = wp_get_current_user();
        $is_mod = 'no';
        for($i=0;$i<=5;$i++){
            $mod = 'weca_event_mod_email_'.$i;
            if($user->user_email==$event->$mod){
                $is_mod = 'yes';
                break;
            }

        }
        
      
        $data = array(            
            "weca_event_id" => $event_id,
            "user_id" => $user->ID,
            "user_email" => $user->user_email,
            "user_login_time" => date('h:i:s'),
            "user_login_delay" => date('h:i:s'),
            "user_page_out_time" => date('h:i:s'),
            "user_page_out_missed" => date('h:i:s'),
            "user_log_out_time" => date('h:i:s'),
            "user_log_out_missed" => date('h:i:s'),
            "user_event_page_duration" => date('h:i:s'), 
            "user_other_pages" => 'none',
            "user_silence_time" => date('h:i:s'),
            "user_moderator" => $is_mod,
            "user_moderator_in_date" => date('Y-m-d'),
            "user_moderator_in_time" => date('h:i:s'),
            "user_moderator_out_date" => date('Y-m-d'),
            "user_moderator_out_time" => date('h:i:s')
        );
        $wpdb->insert($wpdb->prefix.'weca_analytics_user', $data);
       
        if($is_mod == "yes" || current_user_can('administrator') || $event->weca_event_attendee_list == "visible"){
            $response = array(
                'resp' => true,
                'user' => $user->user_nicename
            );
            
        }
        else{
             $response = array(
                'resp' => false
            
            );
            
        }
        print_r(json_encode($response));
        wp_die();
       
        
        
       
    }
    
    public function silence_user_callback(){
        global $wpdb;
        $event_id = $_POST['event_id'];
        $user_id = $_POST['user_id'];
        $key = "user_silence_from_".$event_id;
        if(get_user_meta($user_id, $key, true) == ""){
            add_user_meta( $user_id, 'user_silence_from_event', $event_id);
            $data = array(
                'user_silence_time'=>date('h:i:s')
            );
            $wpdb->update($wpdb->prefix."weca_analytics_user", $data,  array("weca_event_id"=>$event_id, "user_id"=>$user_id));
        }
       
        

        
    }
    
    public function get_total_minutes($days, $hours, $mins){
        $total_minutes_in_days = $days * 24 * 60;
        $total_minutes_in_hours = $hours * 60;
        return $total_minutes_in_days + $total_minutes_in_hours + $mins;
        
    }
    
    public function add_sticky_message(){
        global $wpdb;
        $user = wp_get_current_user();
        $comment_array = array(
            'weca_event_id' => $_POST['event_id'],
            'text_datetime' => strtotime(date('Y-m-d h:i:s A')),
            'text_room_status' => '',
            'text_by_user_id' => $user->ID,
            'text_type' => '',
            'text_message' => $_POST['message'],
            'text_sticky' => "yes",
            'text_url'  => '',
            'text_promoted' => '',
            'text_silenced' => '',
            'text_sticky_in_date' => date('Y-m-d'),
            'text_sticky_in_time' => date('h:i:s A'),
            'text_sticky_out_date' => '',
            'text_sticky_out_time' => ''

        );  

        $wpdb->insert($wpdb->prefix.'weca_chat_log', $comment_array);
        echo $user->user_nicename;
        wp_die();
        
    }
    
    public function check_for_changes(){
        global $wpdb;
        $event_id = $_POST['event_id'];
        $chat_room = $_POST['chat_room'];
        $total_comments = $_POST['total_comments'];
    
        $event_query = "SELECT * FROM ".$wpdb->prefix."weca_events WHERE weca_event_id = '".$event_id."';";
        $event = $wpdb->get_row($event_query);
        
        $mod_comment_query = "SELECT user.user_nicename AS username, chat.text_message AS message FROM ".$wpdb->prefix."weca_chat_log AS chat LEFT JOIN wp_users AS user ON user.ID = chat.text_by_user_id WHERE weca_event_id = '".$event->weca_event_id."' AND text_sticky='yes'";
        $mod_comments = $wpdb->get_results($mod_comment_query);

        $chatroom = false;
        $refresh - false;
        $html = "";
       
        if(sizeof($mod_comments) != $total_comments){
            $refresh = true;
            foreach(array_reverse($mod_comments) as $comment){
                $html .= "<li><span>".$comment->username.":</span>".$comment->message."</li>";
            }
         
        }
        $response = array(
            "refresh" => $refresh,
            "chatroom_status" => $event->weca_chat_room,
            "html" => $html

        );
        echo json_encode($response);
        wp_die();
        
    }
    
    public function after_logout(){
       
        global $wpdb;
       
        $user = wp_get_current_user();
        $event_query = "SELECT wp_weca_events.weca_event_id,wp_weca_events.weca_event_page_id,wp_weca_analytics_user.user_id,  wp_weca_events.weca_event_end_date, wp_weca_events.weca_event_end_time FROM `wp_weca_events` LEFT JOIN wp_weca_analytics_user ON wp_weca_events.weca_event_id = wp_weca_analytics_user.weca_event_id WHERE wp_weca_analytics_user.user_id = ".$user->ID;
        $events = $wpdb->get_results($event_query);
        foreach($events as $event){
            $event_end_time = new DateTime($event->weca_event_end_date.' '.$event->weca_event_end_time);
            $current_date = new DateTime();
            
            $logout_delay = date_diff($current_date, $event_end_time);
            $logout_delay->format('%a %h %i %s');
           
            
            if($logout_delay->invert == 0){
                 $logout_missed = $logout_delay->days.':'.$logout_delay->h.':'.$logout_delay->i.':'.$logout_delay->s;
            }
            else{
                $logout_missed = "00:00:00:00";
            }
            
            $data = array(
                 'user_log_out_time'=>date('h:i:s'),
                 'user_log_out_missed'=>$logout_missed
            );
            if($event->weca_event_page_id == $_COOKIE['event_id']){
                $newdata = array(
                    'user_page_out_missed' => $logout_missed,
                    'user_page_out_time'  => date('h:i:s')
                    );
                $data = array_merge($newdata, $data);
                setcookie('event_id', $_COOKIE['event_id'], time()-1, "/");
            }
            
            $wpdb->update($wpdb->prefix."weca_analytics_user", $data,  array("weca_event_id"=>$event->weca_event_id, "user_id"=>$user->ID));
            
        }
        delete_user_meta($user->ID, 'event_joined');
        
    }
    public function after_login(){

        global $wpdb;

        $user = wp_get_current_user();
        $event_query = "SELECT wp_weca_events.weca_event_id,wp_weca_analytics_user.user_id,  wp_weca_events.weca_event_start_date, wp_weca_events.weca_event_start_time FROM `wp_weca_events` LEFT JOIN wp_weca_analytics_user ON wp_weca_events.weca_event_id = wp_weca_analytics_user.weca_event_id WHERE wp_weca_analytics_user.user_id = ".$user->ID;
        $events = $wpdb->get_results($event_query);
        foreach($events as $event){
            $event_start_time = new DateTime($event->weca_event_start_date.' '.$event->weca_event_start_time);
            $current_date = new DateTime();
            
            $login_delay = date_diff($current_date, $event_start_time);
            $login_delay->format('%a %h %i %s');
            

            if($login_delay->invert == 0){
                 $login_missed = $login_delay->days.':'.$login_delay->h.':'.$login_delay->i.':'.$login_delay->s;
            }
            else{
                $login_missed = "00:00:00:00";
            }
            
            $data = array(
                     'user_login_time'=>date('h:i:s'),
                     'user_login_delay'=>$login_missed
                );
            
            $wpdb->update($wpdb->prefix."weca_analytics_user", $data,  array("weca_event_id"=>$event->weca_event_id, "user_id"=>$user->ID));

        }
        
        


    }

    public function on_browser_close(){
        global $wpdb;
        $user = wp_get_current_user();
        $page_id = get_the_ID(); 
        $event = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_events WHERE weca_event_page_id= '.$page_id);        
        
        $event_end_time = new DateTime($event->weca_event_end_date.' '.$event->weca_event_end_time);
        $current_date = new DateTime();

        $pageout_delay = date_diff($current_date, $event_end_time);
        $pageout_delay->format('%a %h %i %s');


        if($pageout_delay->invert == 0){
             $logout_missed = $pageout_delay->days.':'.$pageout_delay->h.':'.$pageout_delay->i.':'.$pageout_delay->s;
        }
        else{
            $logout_missed = "00:00:00:00";
        }
        
      
        $data = array(
            'user_page_out_missed' => $logout_missed,
            'user_page_out_time'  => date('h:i:s')
            );
       
        setcookie('event_id', $_COOKIE['event_id'], time()-1, "/");
        

        $wpdb->update($wpdb->prefix."weca_analytics_user", $data,  array("weca_event_id"=>$event->weca_event_id, "user_id"=>$user->ID));
        delete_user_meta($user->ID, 'event_joined', $event->weca_event_id);

        
    }
    
    public function on_click_url(){
        global $wpdb;
        $page_id = $_COOKIE['event_id']; 
        $event = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'weca_events WHERE weca_event_page_id= '.$page_id);
        
        $user = wp_get_current_user();
        $data = array(
            'click_by_user_id' => $user->ID,
            'weca_event_id' => $event->weca_event_id,
            'click_url' => $_POST['href']
            
        );
        
            
        
        
        $wpdb->insert($wpdb->prefix.'weca_click_log', $data);
      
        
    }
    
    public function testcron(){
        global $wpdb;
        $events = $wpdb->get_results('SELECT * from '.$wpdb->prefix.'weca_events');
        $time_array = array(15,30,45,60,75,90,105,120,135,150,165,180,195,210,225,250);
        
        foreach($events as $event){
            $event_start_time = strtotime($event->weca_event_start_date.' '.$event->weca_event_start_time);
            $current_date = strtotime(date('Y-m-d h:i:s'));            
            $total_minutes = round(abs($event_start_time - $current_date) / 60,0);
            
            if (in_array($total_minutes, $time_array)){
                $time = $total_minutes;
                if($total_minutes < 100){
                    $time = '0'.$total_minutes;
                }
                
                $totalusers = $wpdb->get_results('SELECT COUNT(*) as count from '.$wpdb->prefix.'usermeta WHERE meta_key="event_joined" AND meta_value='.$event->weca_event_id);
                $data = array(
                    'weca_event_id' => $event->weca_event_id,
                    'analytics_attendance_'.$time => $totalusers[0]->count
                );               
              
                $record_exists = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."weca_attendance_analytics WHERE weca_event_id = $event->weca_event_id", ARRAY_A);
                if($record_exists == ""){
                    $data['analytics_attendance_peak'] =date('Y-m-d h:i:s');
                    
                    $wpdb->insert($wpdb->prefix."weca_attendance_analytics", $data);               
                     
                }
                else{
                    unset($record_exists['weca_event_id']);
                    unset($record_exists['analytics_attendance_peak']); 
                    unset($record_exists['analytics_attendance_'.$time]);
                    
                    $highest = max(array_values($record_exists));
                  
                    
                    if($highest < $totalusers[0]->count){
                        $data['analytics_attendance_peak'] =date('Y-m-d h:i:s');
                    }
                    $wpdb->update($wpdb->prefix."weca_attendance_analytics", $data,array('weca_event_id' => $event->weca_event_id));
                    
                }
                
                
            }
            
            
            
        }
        
    }
   
    
       
    
   
    
}