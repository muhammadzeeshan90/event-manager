<?php

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class My_List_Table extends WP_List_Table {

    
    function get_columns(){
        $columns = array(
            'weca_event_id' => 'Event ID',
            'weca_event_title' => 'Title',
            'weca_event_start_date'    => 'Start Date',
            'weca_event_start_time'      => 'Start Time',
            'weca_event_end_date'   => 'End Date',
            'weca_event_end_time'   => 'End Time',
            'guid' => 'Event Link'
        );
        return $columns;
    }

    function prepare_items() {
        global $wpdb;
        $events = $wpdb->get_results( 'SELECT page.guid, event.weca_event_id, event.weca_event_title, event.weca_event_start_date, event.weca_event_start_time, event.weca_event_end_date, event.weca_event_end_time FROM wp_weca_events as event LEFT JOIN wp_posts as page ON page.ID = event.weca_event_page_id', ARRAY_A  );


        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items =$events;
    }
    
    function column_default( $item, $column_name ) {
          switch( $column_name ) { 
              case 'weca_event_id':
              case 'weca_event_title':
              case 'weca_event_start_date':
              case 'weca_event_start_time':
              case 'weca_event_end_date';
              case 'weca_event_end_time':
              case 'guid':
              return $item[ $column_name ];
            default:
              return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
          }
    }
    
    function column_weca_event_title($item) {
      $actions = array(
                'edit'      => sprintf('<a href="?page=%s&action=%s&event=%s">Edit</a>','add-events','edit',$item['weca_event_id']),
                'delete'    => sprintf('<a href="?page=%s&action=%s&event=%s">Delete</a>','events','delete',$item['weca_event_id'])
            );

      return sprintf('%1$s %2$s', $item['weca_event_title'], $this->row_actions($actions) );
    }
    


}